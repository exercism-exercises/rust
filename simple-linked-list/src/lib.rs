use std::mem::replace;

/// http://rust-unofficial.github.io/too-many-lists/ so useful!!
pub struct SimpleLinkedList<T> {
    //len: usize, optimization; will not implement here
    size: usize,
    head: Option<Box<Node<T>>>,
}

#[derive(Clone)]
pub struct Node<T> {
    element: T,
    next: Option<Box<Node<T>>>,
}

impl<T> SimpleLinkedList<T> {
    pub fn new() -> Self {
        Self {
            size: 0,
            head: None,
        }
    }

    // You may be wondering why it's necessary to have is_empty()
    // when it can easily be determined from len().
    // It's good custom to have both because len() can be expensive for some types,
    // whereas is_empty() is almost always cheap.
    // (Also ask yourself whether len() is expensive for SimpleLinkedList)
    pub fn is_empty(&self) -> bool {
        match self.head {
            None => true,
            _ => false,
        }
    }

    pub fn len(&self) -> usize {
        self.size
    }

    pub fn push(&mut self, element: T) {
        self.size += 1;
        let new_node = Some(Box::new(Node::<T> {
            element: element,
            next: replace(&mut self.head, None),
        }));
        self.head = new_node;
    }

    pub fn pop(&mut self) -> Option<T> {
        let head = replace(&mut self.head, None);

        match head {
            None => None,
            Some(node_box) => {
                self.size -= 1;
                let node = *node_box;
                self.head = node.next;
                Some(node.element)
            }
        }
    }

    pub fn peek(&self) -> Option<&T> {
        // option has as_ref also.
        self.head.as_ref().map(|node| &node.element)
    }

    #[must_use]
    pub fn rev(self) -> SimpleLinkedList<T> {
        let mut list = self;
        let mut reversed_list = SimpleLinkedList::<T>::new();
        while let Some(element) = list.pop() {
            reversed_list.push(element);
        }
        reversed_list
    }
}

impl<T> FromIterator<T> for SimpleLinkedList<T> {
    fn from_iter<I: IntoIterator<Item = T>>(iter: I) -> Self {
        let mut list = SimpleLinkedList::<T>::new();
        for element in iter {
            list.push(element);
        }
        list
    }
}

pub struct SimpleLinkedListIterator<T> {
    list: SimpleLinkedList<T>
}

impl<T> Iterator for SimpleLinkedListIterator<T> {
    type Item = T;
    fn next(&mut self) -> Option<Self::Item> {
        self.list.pop()
    }
}


// and we'll implement IntoIterator
impl<T> IntoIterator for SimpleLinkedList<T> {
    type Item = T;
    type IntoIter = SimpleLinkedListIterator<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        Self::IntoIter {
            list: self.rev()
        }
    }
}

// In general, it would be preferable to implement IntoIterator for SimpleLinkedList<T>
// instead of implementing an explicit conversion to a vector. This is because, together,
// FromIterator and IntoIterator enable conversion between arbitrary collections.
// Given that implementation, converting to a vector is trivial:
//
// let vec: Vec<_> = simple_linked_list.into_iter().collect();
//
// The reason this exercise's API includes an explicit conversion to Vec<T> instead
// of IntoIterator is that implementing that interface is fairly complicated, and
// demands more of the student than we expect at this point in the track.

impl<T> From<SimpleLinkedList<T>> for Vec<T> {
    fn from(mut linked_list: SimpleLinkedList<T>) -> Vec<T> {
        linked_list.into_iter().collect()
    }
}
