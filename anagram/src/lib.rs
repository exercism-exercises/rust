use std::collections::HashSet;
use std::collections::HashMap;

pub fn anagrams_for<'a>(word: &str, possible_anagrams: &'a[&str]) -> HashSet<&'a str> {
    let original_map = get_map(word);
    possible_anagrams.iter()
                    .filter(
                        |&anagram| 
                        (String::from(anagram.to_lowercase()) != String::from(word.to_lowercase()))
                        && (original_map == get_map(anagram))
                    )
                    .map(|&anagram| anagram)
                    .collect::<HashSet<&'a str>>()
}

fn get_map(word: &str) -> HashMap<char, i32> {
    word.to_lowercase()
        .chars()
        .fold(
            HashMap::new(),
            |mut map, c| {
                *map.entry(c).or_insert(0) += 1; map
            }
        )
}
