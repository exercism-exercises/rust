/// TODO: consider using ndarray or ndalgebra
pub fn find_saddle_points(input: &[Vec<u64>]) -> Vec<(usize, usize)> {
    let mut result: Vec<(usize, usize)> = Vec::new();

    let m: usize = input.len();
    if m > 0 {
        let n: usize = input[0].len();
        if n > 0 {
            let mut max_entries_per_row: Vec<u64> = Vec::new();
            let mut min_entries_per_col: Vec<u64> = Vec::new();

            for r in 0..m {
                max_entries_per_row.push(input[r].iter().cloned().max().unwrap());
            }

            for c in 0..n {
                let mut min_entry_per_col = input[0][c];
                for r in 1..m {
                    if input[r][c] < min_entry_per_col {
                        min_entry_per_col = input[r][c];
                    }
                }
                min_entries_per_col.push(min_entry_per_col);
            }

            for r in 0..m {
                for c in 0..n {
                    let val = input[r][c];
                    if max_entries_per_row[r] <= val && min_entries_per_col[c] >= val {
                        result.push((r, c));
                    }
                }
            }
        }
    }

    result
}
