/// While the problem description indicates a return status of 1 should be returned on errors,
/// it is much more common to return a `Result`, so we provide an error type for the result here.
#[derive(Debug, Eq, PartialEq)]
pub enum AffineCipherError {
    NotCoprime(i32),
}

fn extended_euclidean(x: i32, y: i32) -> (i32, i32, i32) {
    if x == 0 {
        return (y, 0, 1);
    }
    let (gcd, a, b) = extended_euclidean(y % x, x);
    // gcd(x, y) = gcd(y % x, x) = a*(y%x) + b*x = a*(y - floor(y/x)*x) + b*x
    // ->
    //gcd(x, y) = (b-floor(y/x)*a)*x + a*y
    (gcd, b - (y / x) * a, a)
}

/// Encodes the plaintext using the affine cipher with key (`a`, `b`). Note that, rather than
/// returning a return code, the more common convention in Rust is to return a `Result`.
pub fn encode(plaintext: &str, a: i32, b: i32) -> Result<String, AffineCipherError> {
    let is_coprime = extended_euclidean(a, 26).0 == 1;
    if !is_coprime {
        return Err(AffineCipherError::NotCoprime(a));
    }
    let plaintext = plaintext
        .chars()
        .filter(|c| c.is_alphanumeric())
        .collect::<String>();
    let mut result: String = String::new();
    for (u, c) in plaintext.char_indices() {
        if c.is_alphabetic() {
            // E(x) = (ai + b) mod m
            let encoded_letter = (((a * ((c.to_ascii_lowercase() as u8 - 'a' as u8) as i32) + b)
                % 26) as u8
                + 'a' as u8) as char;
            result.push(encoded_letter);
        } else if c.is_digit(10) {
            result.push(c);
        }
        if !result.is_empty() && result.len() % 6 == 5 && u != plaintext.len() - 1 {
            result.push(' ');
        }
    }
    Ok(result)
}

/// Decodes the ciphertext using the affine cipher with key (`a`, `b`). Note that, rather than
/// returning a return code, the more common convention in Rust is to return a `Result`.
pub fn decode(ciphertext: &str, a: i32, b: i32) -> Result<String, AffineCipherError> {
    let (gcd, x, _) = extended_euclidean(a, 26);
    if gcd != 1 {
        return Err(AffineCipherError::NotCoprime(a));
    }
    let mut result: String = String::new();

    let result: String = ciphertext
        .chars()
        .filter(|c| c.is_alphanumeric())
        .map(|c| {
            if c.is_digit(10) {
                c
            } else {
                let mut y = (c as u8 - 'a' as u8) as i32;
                y -= b;
                y %= 26;
                y += 26;
                y %= 26;
                y *= (x + 26) % 26;
                y += 26;
                y %= 26;
                let decoded_char = (y as u8 + 'a' as u8) as char;
                decoded_char
            }
        })
        .collect();
    //D(y) = (a^-1)(y - b) mod m
    Ok(result)
}
