use std::cmp::Ordering;

#[derive(Debug, PartialEq, Eq)]
pub enum Classification {
    Abundant,
    Perfect,
    Deficient,
}

pub fn classify(num: u64) -> Option<Classification> {
    if num == 0 {
        return None;
    }

    // unlike prime number factorization, we don't need to divide with prime factors multiple times
    let aliquot_sum = (1u64..num).filter(|u| num % u == 0).sum::<u64>();
    let classification: Classification = match num.cmp(&aliquot_sum) {
        Ordering::Less => Classification::Abundant,
        Ordering::Equal => Classification::Perfect,
        Ordering::Greater => Classification::Deficient
    };
    Some(classification)
}
