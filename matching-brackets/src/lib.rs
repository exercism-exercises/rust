const OPEN_PARENTHESIS: char = '(';
const OPEN_CURLY_BRACKET: char = '{';
const OPEN_BRACKET: char = '[';

const CLOSE_PARENTHESIS: char = ')';
const CLOSE_CURLY_BRACKET: char = '}';
const CLOSE_BRACKET: char = ']';

fn from_close_to_open(close: char) -> Option<char> {
    match close {
        CLOSE_PARENTHESIS => Some(OPEN_PARENTHESIS),
        CLOSE_CURLY_BRACKET => Some(OPEN_CURLY_BRACKET),
        CLOSE_BRACKET => Some(OPEN_BRACKET),
        _ => None,
    }
}

pub fn brackets_are_balanced(string: &str) -> bool {
    let mut stack: Vec<char> = Vec::new();
    let mut is_balanced: bool = true;
    for c in string.chars() {
        if c == CLOSE_PARENTHESIS || c == CLOSE_BRACKET || c == CLOSE_CURLY_BRACKET {
            is_balanced = match (from_close_to_open(c), stack.pop()) {
                (Some(a), Some(b)) => a == b,
                _ => false,
            };
            if !is_balanced {
                break;
            }
        } else if c == OPEN_PARENTHESIS || c == OPEN_BRACKET || c == OPEN_CURLY_BRACKET {
            stack.push(c);
        }
        
    }
    stack.is_empty() && is_balanced
}
