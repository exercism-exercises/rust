pub struct Triangle {
    a: u64,
    b: u64,
    c: u64,
}

impl Triangle {
    pub fn build(sides: [u64; 3]) -> Option<Triangle> {
        let is_c_valid = sides[0] + sides[1] >= sides[2];
        let is_b_valid = sides[0] + sides[2] >= sides[1];
        let is_a_valid = sides[1] + sides[2] >= sides[0];

        if is_a_valid && is_b_valid && is_c_valid && sides[0] > 0 && sides[1] > 0 && sides[2] > 0 {
            Some(Triangle{a: sides[0], b: sides[1], c: sides[2]})
        } else {
            None
        }
    }

    pub fn is_equilateral(&self) -> bool {
        self.a == self.b && self.b == self.c
    }

    pub fn is_scalene(&self) -> bool {
        self.a != self.b && self.b != self.c && self.c != self.a
    }

    pub fn is_isosceles(&self) -> bool {
        self.a == self.b || self.b == self.c || self.c == self.a
    }
}
