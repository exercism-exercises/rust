use std::collections::HashMap;

#[derive(Clone, PartialEq, Eq, Hash)]
enum Suit {
    Spade,
    Heart,
    Club,
    Diamond,
}
#[derive(Clone, Hash, PartialOrd, Ord, PartialEq, Eq)]
enum HandType {
    HighCard,          // this is not a concern as we will provide ordered cards of a hand
    OnePair(u8),       // we keep track of the rank to break ties
    TwoPair(u8, u8),   // we keep track of both ranks of the pairs to break ties
    ThreeOfAKind(u8),  // like one pair
    Straight(u8),      // we keep track of the max rank to break ties
    Flush,             // like high card, we will provide ordered cards of a hand and that will do
    FullHouse(u8, u8), // we keep track of both ranks, triple and pair
    FourOfAKind(u8),   // like three of a kind
    StraightFlush(u8), // like straight
}

type Card = (u8, Suit);
type Hand = (HandType, [u8; 5]);

fn parse(hand: &str) -> Vec<Card> {
    hand.split_ascii_whitespace()
        .map(|card| {
            let card: Vec<char> = card.chars().collect();
            let n = card.len();
            assert!(n == 2 || n == 3);
            let rank = match (card[0], card[1]) {
                // rank card accordingly
                ('A', _) => 14,
                ('K', _) => 13,
                ('Q', _) => 12,
                ('J', _) => 11,
                ('1', '0') => 10,
                ('9', _) => 9,
                ('8', _) => 8,
                ('7', _) => 7,
                ('6', _) => 6,
                ('5', _) => 5,
                ('4', _) => 4,
                ('3', _) => 3,
                ('2', _) => 2,
                _ => panic!(),
            };

            let suit = match card.last().unwrap() {
                'S' => Suit::Spade,
                'H' => Suit::Heart,
                'C' => Suit::Club,
                'D' => Suit::Diamond,
                _ => panic!(),
            };

            (rank, suit)
        })
        .collect()
}

fn evaluate(cards: Vec<Card>) -> Hand {
    assert_eq!(cards.len(), 5);
    let mut hand_types: Vec<HandType> = vec![HandType::HighCard];
    let mut rank_to_count: HashMap<u8, usize> = HashMap::new();
    let mut suit_to_count: HashMap<Suit, usize> = HashMap::new();
    for (rank, suit) in cards.iter() {
        *rank_to_count.entry(*rank).or_insert(0) += 1;
        *suit_to_count.entry(suit.clone()).or_insert(0) += 1;
    }

    // get pairs and kinds
    let mut ranks: Vec<(u8, usize)> = rank_to_count.into_iter().collect();
    ranks.sort_unstable_by_key(|(_, count)| *count);
    ranks.reverse();

    match (ranks[0], ranks[1]) {
        ((x, 4), _) => hand_types.push(HandType::FourOfAKind(x)),
        ((x, 3), (y, 2)) => hand_types.push(HandType::FullHouse(x, y)),
        ((x, 3), _) => hand_types.push(HandType::ThreeOfAKind(x)),
        ((x, 2), (y, 2)) => hand_types.push(HandType::TwoPair(x.max(y), x.min(y))),
        ((x, 2), _) => hand_types.push(HandType::OnePair(x)),
        _ => (),
    }

    let is_flush = suit_to_count.values().any(|&v| v == 5);
    let mut unsorted_rank_entries = cards.iter().map(|&(rank, _)| rank).collect::<Vec<u8>>();
    unsorted_rank_entries.sort_unstable();
    unsorted_rank_entries.reverse();
    let rank_entries: [u8; 5] = unsorted_rank_entries.try_into().unwrap();
    let is_straight = (rank_entries[0] - rank_entries[1] == 1
        || rank_entries[0] == 14 && rank_entries[1] == 5)
        && (1usize..4).all(|u| rank_entries[u] - rank_entries[u + 1] == 1);

    match (is_straight, is_flush) {
        (true, true) => hand_types.push(HandType::StraightFlush(if rank_entries[0] == 14 {
            rank_entries[1]
        } else {
            rank_entries[0]
        })),
        (true, false) => hand_types.push(HandType::Straight(if rank_entries[0] == 14 {
            rank_entries[1]
        } else {
            rank_entries[0]
        })),
        (false, true) => hand_types.push(HandType::Flush),
        (_, _) => (),
    }
    let hand_type = hand_types.iter().max().unwrap().clone();
    (hand_type, rank_entries)
}

/// Given a list of poker hands, return a list of those hands which win.
///
/// Note the type signature: this function should return _the same_ reference to
/// the winning hand(s) as were passed in, not reconstructed strings which happen to be equal.
pub fn winning_hands<'a>(hands: &[&'a str]) -> Vec<&'a str> {
    if hands.len() == 1 {
        Vec::from(hands)
    } else {
        let evaluated: Vec<(Hand, &'a str)> = hands
            .iter()
            .map(|&hand| (evaluate(parse(hand)), hand))
            .collect();
        let max_hand = evaluated
            .clone()
            .iter()
            .map(|(hand, _)| hand)
            .max()
            .unwrap()
            .clone();

        evaluated
            .iter()
            .cloned()
            .filter_map(|(hand, entry)| if hand == max_hand { Some(entry) } else { None })
            .collect()
    }
}
