use std::cmp::Ordering;
use std::collections::HashMap;

pub struct Matrix {
    // Implement your Matrix struct
    adjacency: HashMap<(usize, usize), u32>,
    total_row_no: usize,
    total_col_no: usize,
}

impl Matrix {
    pub fn new(input: &str) -> Self {
        let adjacency: HashMap<(usize, usize), u32> = input
            .split('\n')
            .enumerate()
            .flat_map(|(row_no, row)| {
                row.split(' ').enumerate().map(move |(col_no, value)| {
                    ((row_no + 1, col_no + 1), value.trim().parse().unwrap())
                })
            })
            .collect();

        let &(total_row_no, total_col_no) = adjacency
            .keys()
            .max_by(|(x1, x2), (y1, y2)| match x1.cmp(y1) {
                Ordering::Equal => x2.cmp(y2),
                x => x,
            })
            .unwrap();

        Self {
            adjacency,
            total_row_no,
            total_col_no,
        }
    }

    pub fn row(&self, row_no: usize) -> Option<Vec<u32>> {
        match row_no {
            y if y < 1 || y > self.total_row_no => None,
            _ => Some(
                (1..=self.total_col_no)
                    .map(|col_no| self.adjacency[&(row_no, col_no)])
                    .collect(),
            ),
        }
    }

    pub fn column(&self, col_no: usize) -> Option<Vec<u32>> {
        match col_no {
            x if x < 1 || x > self.total_col_no => None,
            _ => Some(
                (1..=self.total_row_no)
                    .map(|row_no| self.adjacency[&(row_no, col_no)])
                    .collect(),
            ),
        }
    }
}
