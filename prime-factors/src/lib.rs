pub fn factors(n: u64) -> Vec<u64> {
    let mut m: u64 = n;
    let mut factors = Vec::<u64>::new();
    for i in 2.. {
        if m < i {
            break
        }

        while m % i == 0 {
            factors.push(i);
            m /= i;
        }
    }
    factors
}
