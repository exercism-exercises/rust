use std::collections::HashMap;
use std::ops::Add;

///
/// Team                           | MP |  W |  D |  L |  P
/// Devastating Donkeys            |  3 |  2 |  1 |  0 |  7
/// Allegoric Alaskans             |  3 |  2 |  0 |  1 |  6
/// Blithering Badgers             |  3 |  1 |  0 |  2 |  3
/// Courageous Californians        |  3 |  0 |  1 |  2 |  1
#[derive(Clone, Copy, Debug, Default, PartialEq)]
struct Team {
    games: u32,
    win: u32,
    draw: u32,
    loss: u32,
    points: u32,
}

impl Add for Team {
    type Output = Team;

    fn add(self, other: Team) -> Team {
        Team {
            games: self.games + other.games,
            win: self.win + other.win,
            draw: self.draw + other.draw,
            loss: self.loss + other.loss,
            points: self.points + other.points,
        }
    }
}

const WIN: &str = "win";
const DRAW: &str = "draw";
const LOSS: &str = "loss";

pub fn tally(match_results: &str) -> String {
    let mut table: HashMap<String, Team> = HashMap::new();
    for game_line in match_results.lines() {
        let game = game_line.split(";").collect::<Vec<&str>>();
        assert_eq!(3, game.len());
        let home_team = game[0];
        let away_team = game[1];
        let result = game[2];
        let (home_team_update, away_team_update) = match result {
            WIN => (
                Team {
                    games: 1,
                    win: 1,
                    draw: 0,
                    loss: 0,
                    points: 3,
                },
                Team {
                    games: 1,
                    win: 0,
                    draw: 0,
                    loss: 1,
                    points: 0,
                },
            ),
            DRAW => (
                Team {
                    games: 1,
                    win: 0,
                    draw: 1,
                    loss: 0,
                    points: 1,
                },
                Team {
                    games: 1,
                    win: 0,
                    draw: 1,
                    loss: 0,
                    points: 1,
                },
            ),
            LOSS => (
                Team {
                    games: 1,
                    win: 0,
                    draw: 0,
                    loss: 1,
                    points: 0,
                },
                Team {
                    games: 1,
                    win: 1,
                    draw: 0,
                    loss: 0,
                    points: 3,
                },
            ),
            _ => panic!("result should be one of win, draw, or loss"),
        };
        if !table.contains_key(&home_team.to_string()) {
            table.insert(home_team.to_string(), Team::default());
        }
        if !table.contains_key(&away_team.to_string()) {
            table.insert(away_team.to_string(), Team::default());
        }
        table.insert(
            home_team.to_string(),
            table[&home_team.to_string()] + home_team_update,
        );
        table.insert(
            away_team.to_string(),
            table[&away_team.to_string()] + away_team_update,
        );
    }

    let mut v: Vec<_> = table.into_iter().collect();
    v.sort_by(|x, y| match y.1.points.cmp(&x.1.points) {
        std::cmp::Ordering::Equal => x.0.cmp(&y.0),
        o => o,
    });
    v.iter()
        .map(|(name, team)| {
            format!(
                "{:<30} | {:>2} | {:>2} | {:>2} | {:>2} | {:>2}",
                name, team.games, team.win, team.draw, team.loss, team.points
            )
        })
        .fold(
            String::from("Team                           | MP |  W |  D |  L |  P"),
            |s1, s2| s1 + "\n" + s2.as_str(),
        )
}
