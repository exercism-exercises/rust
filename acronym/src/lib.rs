const SPACE: char = ' ';
const HYPHEN: char = '-';
/// Creates an acronym from a given phrase.
/// It ignores all punctuations but hyphen.
/// Complementary metal-oxide semiconductor -> CMOS
/// It also considers only the first character of an ALL-CAPS word
/// GNU Image Manipulation Program -> GIMP
pub fn abbreviate(phrase: &str) -> String {
    // 1. initialization -- one char to store previous char and one string for the result
    let mut prev: char = SPACE;
    let mut result = String::new();
    // 2. iteration
    for c in phrase.chars() {
        // 2.a. skip the char is a punctuation, with the exception of hyphen
        if c != HYPHEN && c.is_ascii_punctuation() {
            continue;
        }
        // 2.b. push to result if
        // - beginning of a lower-cased/upper-cased word following a whitespace or a hyphen
        // - beginning of an upper case char following a lower case char (camel case)
        if c != SPACE && c != HYPHEN && (prev == SPACE || prev == HYPHEN || (prev.is_lowercase() && c.is_uppercase())) {
            result.push(c.clone());
        }
        // 2.c. keep track of previously pushed char/whitespace/hyphen
        prev = c;
    }
    // 3. output -- return the result in ALL CAPS.
    result.to_uppercase()
}
