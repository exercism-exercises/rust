pub struct Allergies {
    score: u32,
}

/// Enum for allergens
/// had to include copy and clone
#[derive(Debug, PartialEq, Eq, Copy, Clone)]
pub enum Allergen {
    Eggs,
    Peanuts,
    Shellfish,
    Strawberries,
    Tomatoes,
    Chocolate,
    Pollen,
    Cats,
}

impl From<u32> for Allergen {
    fn from(orig: u32) -> Self {
        match orig {
            0 => Allergen::Eggs,
            1 => Allergen::Peanuts,
            2 => Allergen::Shellfish,
            3 => Allergen::Strawberries,
            4 => Allergen::Tomatoes,
            5 => Allergen::Chocolate,
            6 => Allergen::Pollen,
            7 => Allergen::Cats,
            _ => panic!(""),
        }
    }
}

impl Allergies {
    pub fn new(score: u32) -> Self {
        Self {
            score: score,
        }
    }

    pub fn is_allergic_to(&self, allergen: &Allergen) -> bool {
        let allergen_score: u32 = 1 << *allergen as u32;
        (self.score & allergen_score) > 0
    }

    pub fn allergies(&self) -> Vec<Allergen> {
        (0..8)
            .map(Allergen::from)
            .filter(|a| self.is_allergic_to(&a))
            .collect::<Vec<Allergen>>()
    }
}
