const TEN: u32 = 10;

pub trait Luhn {
    fn valid_luhn(&self) -> bool;
}

/// Here is the example of how to implement custom Luhn trait
/// for the &str type. Naturally, you can implement this trait
/// by hand for the every other type presented in the test suite,
/// but your solution will fail if a new type is presented.
/// Perhaps there exists a better solution for this problem?
impl<'a> Luhn for &'a str {
    fn valid_luhn(&self) -> bool {
        let digits = self
            .chars()
            .filter(|c| !c.is_whitespace())
            .map(|c| c.to_digit(TEN))
            .collect::<Vec<Option<u32>>>();

        let filtered_digits = digits.iter().flatten().cloned().collect::<Vec<u32>>();
        let size = digits.len();
        if size != filtered_digits.len() || size < 2 {
            return false;
        }
        let mut result: u32 = 0;

        for (i, &d) in filtered_digits.iter().rev().enumerate() {
            if i % 2 == 1 {
                result += if d > 4 { 2 * d - 9 } else { 2 * d };
            } else {
                result += d;
            }
        }
        result % 10 == 0
    }
}

impl Luhn for String {
    fn valid_luhn(&self) -> bool {
        self.clone().as_str().valid_luhn()
    }
}

macro_rules! impl_luhn_for_numbers {
    ( $($type:ty),* $(,)? ) => {
        $(
            impl Luhn for $type {
                fn valid_luhn(&self) -> bool {
                    let mut number: $type = self.clone();
                    let mut vec: Vec<$type> = Vec::new();
                    while number > 0 {
                        vec.push(number % 10);
                        number /= 10;
                    }
                    let mut result: $type = 0;
                    for (i, &d) in vec.iter().enumerate() {
                        if i % 2 == 1 {
                            result += if d > 4 { 2 * d - 9 } else { 2 * d };
                        } else {
                            result += d;
                        }
                    }
                    result % 10 == 0
                }

            }
        )*
    };
}

impl_luhn_for_numbers!(u8, u16, u32, u64, u128, usize);
