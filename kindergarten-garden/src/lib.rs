use phf::phf_map;
/**
 * - Alice, Bob, Charlie, David,
- Eve, Fred, Ginny, Harriet,
- Ileana, Joseph, Kincaid, and Larry.
 */
static NAMES: phf::Map<&'static str, usize> = phf_map! {
    "Alice" => 0,
    "Bob" => 1,
    "Charlie" => 2,
    "David" => 3,
    "Eve" => 4,
    "Fred" => 5,
    "Ginny" => 6,
    "Harriet" => 7,
    "Ileana" => 8,
    "Joseph" => 9,
    "Kincaid" => 10,
    "Larry" => 11,
};

// grass, clover, radishes, and violets
static PLANTS: phf::Map<char, &'static str> = phf_map! {
    'C' => "clover",
    'G' => "grass",
    'R' => "radishes",
    'V' => "violets",
};

pub fn plants(diagram: &str, student: &str) -> Vec<&'static str> {
    let index = NAMES[student];
    diagram
        .split("\n")
        .map(|s| {
            s.chars()
                .enumerate()
                .filter(|(i, c)| i / 2 == index)
                .map(|(i, c)| PLANTS[&c])
        })
        .flatten()
        .collect::<Vec<&'static str>>()
}
