/// Custom errors to return is here.
#[derive(Debug, PartialEq, Eq)]
pub enum Error {
    NotEnoughPinsLeft,
    GameComplete,
}

#[derive(Debug, PartialEq, Eq)]
enum Frame {
    Done, // marks done
    Invalid(Error), // invalid
    Strike(usize), // strike with frame label
    Half(u16, usize), // half frame, will change to either regular or spare
    Regular(u16, u16, usize), // regular frame
    Spare(u16, u16, usize),
    SpareFill(u16), // one extra fill for frame 10 ending in a spare
    StrikeFill(u16, bool), // two extra fills for frame 10 ending in a strike
}

/// A bowling game is represented here.
pub struct BowlingGame {
    frames: Vec<Frame>,
}

impl BowlingGame {
    ///This is a constructor.
    pub fn new() -> Self {
        return Self { frames: Vec::new() };
    }

    /// Registers one throw to the bowling game.
    /// 1. Check whether a game is complete
    /// 2.  
    pub fn roll(&mut self, pins: u16) -> Result<(), Error> {
        // from the latest frame determine the new frame
        let is_first_frame = self.frames.is_empty();
        // if empty
        let new_frame: Frame = if is_first_frame {
            // if pins > 10 invalid
            if pins > 10 {
                Frame::Invalid(Error::NotEnoughPinsLeft)
            }
            // if pins == 10 it's a strike, first frame done
            else if pins == 10 {
                Frame::Strike(1)
            }
            // if pins < 10, first half done
            else {
                Frame::Half(pins, 1)
            }
        } else {
            // if at least one frame is played
            match self.frames[self.frames.len() - 1] {
                // if half frame

                // can't score more than what is remaining
                Frame::Half(first_half, _) if first_half + pins > 10 => {
                    Frame::Invalid(Error::NotEnoughPinsLeft)
                }
                // if from frame 1, ..., 10 and finish as spare
                Frame::Half(first_half, frame_number)
                    if frame_number <= 10 && first_half + pins == 10 =>
                {
                    Frame::Spare(first_half, pins, frame_number)
                }
                // if from frame 1, ..., 10 and don't finish as spare
                Frame::Half(first_half, frame_number)
                    if frame_number <= 10 && first_half + pins < 10 =>
                {
                    Frame::Regular(first_half, pins, frame_number)
                }

                // finished a regular frame before.

                // if pins == 10 - a strike
                Frame::Regular(_, _, frame_number) if frame_number < 10 && pins == 10 => {
                    Frame::Strike(frame_number + 1)
                }
                // else a half frame
                Frame::Regular(_, _, frame_number) if frame_number < 10 && pins < 10 => {
                    Frame::Half(pins, frame_number + 1)
                }

                // finished a spare game

                // if the last frame
                Frame::Spare(_, _, frame_number) if frame_number == 10 && pins <= 10 => {
                    Frame::SpareFill(pins)
                }
                // if a strike
                Frame::Spare(_, _, frame_number) if frame_number < 10 && pins == 10 => {
                    Frame::Strike(frame_number + 1)
                }
                // if half frame begins
                Frame::Spare(_, _, frame_number) if frame_number < 10 && pins < 10 => {
                    Frame::Half(pins, frame_number + 1)
                }

                // finished a strike game

                // strikes in a row
                Frame::Strike(frame_number) if pins == 10 && frame_number < 10 => {
                    Frame::Strike(frame_number + 1)
                }
                // extra fill
                Frame::Strike(frame_number) if pins <= 10 && frame_number == 10 => {
                    Frame::StrikeFill(pins, false)
                }
                // a new half frame begins
                Frame::Strike(frame_number) if pins < 10 && frame_number < 10 => {
                    Frame::Half(pins, frame_number + 1)
                }

                Frame::StrikeFill(10, false) if pins <= 10 => Frame::StrikeFill(pins, true),
                Frame::StrikeFill(x, false) if x < 10 && x + pins > 10 => {
                    Frame::Invalid(Error::NotEnoughPinsLeft)
                }
                Frame::StrikeFill(x, false) if x < 10 && x + pins <= 10 => {
                    Frame::StrikeFill(pins, true)
                }

                // can't score over 10 pins at once
                _ if pins > 10 => Frame::Invalid(Error::NotEnoughPinsLeft),
                _ => Frame::Invalid(Error::GameComplete),
            }
        };

        let must_pop = !is_first_frame && {
            match self.frames[self.frames.len() - 1] {
                Frame::Half(_, _) => true,
                _ => false,
            }
        };

        let is_done = match new_frame {
            Frame::Regular(_, _, 10) | Frame::SpareFill(_) | Frame::StrikeFill(_, true) => true,
            _ => false,
        };
        match new_frame {
            Frame::Invalid(err) => Err(err),
            _ => {
                if must_pop {
                    self.frames.pop();
                }
                self.frames.push(new_frame);
                if is_done {
                    self.frames.push(Frame::Done);
                }
                Ok(())
            }
        }
    }

    pub fn score(&self) -> Option<u16> {
        if !self.frames.is_empty() {
            if self.frames[self.frames.len() - 1] == Frame::Done {
                return Some(
                    self.frames
                        .windows(3)
                        //.inspect(|window| println!("{:?}", window))
                        .map(|window| match window {
                            [Frame::Strike(_), Frame::StrikeFill(x1, _), Frame::StrikeFill(x2, _)] => 10 + x1 + x2,
                            [Frame::Strike(_), Frame::Strike(_), Frame::StrikeFill(x, _)] => 20 + x,
                            [Frame::Strike(_), Frame::Regular(x, y, _), Frame::Done] => {
                                10 + 2 * x + 2 * y
                            }
                            [Frame::Strike(_), Frame::Strike(_), Frame::Strike(_)] => 30,
                            [Frame::Strike(_), Frame::Strike(_), Frame::Spare(x, _, _)] => 20 + x,
                            [Frame::Strike(_), Frame::Strike(_), Frame::Regular(x, _, _)] => 20 + x,
                            [Frame::Strike(_), Frame::Spare(x, y, _), _] => 10 + x + y,
                            [Frame::Strike(_), Frame::Regular(x, y, _), _] => 10 + x + y,

                            [Frame::Spare(x1, y1, _), Frame::SpareFill(x2), Frame::Done] => {
                                x1 + y1 + x2
                            }
                            [Frame::Spare(x1, y1, _), Frame::Regular(x2, y2, _), Frame::Done] => {
                                x1 + y1 + 2 * x2 + y2
                            }
                            [Frame::Spare(x, y, _), Frame::Strike(_), _] => x + y + 10,
                            [Frame::Spare(x1, y1, _), Frame::Spare(x2, _, _), _] => x1 + y1 + x2,
                            [Frame::Spare(x1, y1, _), Frame::Regular(x2, _, _), _] => x1 + y1 + x2,

                            [Frame::Regular(x1, y1, _), Frame::Regular(x2, y2, _), Frame::Done] => {
                                x1 + y1 + x2 + y2
                            }
                            [Frame::Regular(x, y, _), _, _] => x + y,
                            _ => 0u16,
                        })
                        .sum(),
                );
            }
        }
        None
    }
}
