use ndarray::Array;

const DIRECTIONS: [(isize, isize); 4] = [(0, 1), (1, 0), (0, -1), (-1, 0)];

pub fn spiral_matrix(size: u32) -> Vec<Vec<u32>> {
    let size: usize = size as usize;
    let mut matrix = Array::zeros((size, size));
    if size > 0 {
        matrix[[0, 0]] = 1;
        let mut previous_location: (usize, usize) = (0, 0);
        let mut d: usize = 0;
        for value in 2..=(size * size) as u32 {
            let current_location = (previous_location.0.checked_add_signed(DIRECTIONS[d].0), previous_location.1.checked_add_signed(DIRECTIONS[d].1));

            d = match current_location {
                (None, _) | (_, None) => (d+1) % 4,
                (Some(x), Some(y)) if x >= size || y >= size => (d+1) % 4,
                (Some(x), Some(y)) if matrix[[x, y]] > 0 => (d+1) % 4,
                _ => d
            };

            let current_location = (previous_location.0.checked_add_signed(DIRECTIONS[d].0).unwrap(), previous_location.1.checked_add_signed(DIRECTIONS[d].1).unwrap());
            matrix[[current_location.0, current_location.1]] = value;
            previous_location = current_location;
        }
    }
    
    matrix
        .rows()
        .into_iter()
        .map(|r| r.to_vec())
        .collect::<Vec<Vec<u32>>>()
}
