#[macro_export]
macro_rules! hashmap {
    () => {
        ::std::collections::HashMap::<_, _>::new()
    };

    ( $( $key:expr => $value:expr ),+ $(,)?) => {{
        let mut map = ::std::collections::HashMap::<_, _>::new();
        $(
                map.insert($key, $value);
        )*
        map
    }};
}
