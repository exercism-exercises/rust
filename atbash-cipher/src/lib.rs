/// "Encipher" with the Atbash cipher.
pub fn encode(plain: &str) -> String {
    let from: String = plain.chars().filter(|c| c.is_alphanumeric()).collect();
    let mut to: String = String::new();
    for (u, c) in from.chars().enumerate() {
        if c.is_alphabetic() {
            to.push(((25u8 - (c.to_ascii_lowercase() as u8 - 'a' as u8))%26 + 'a' as u8) as char);
        } else {
            to.push(c);
        }
        if !to.is_empty() && to.len() % 6 == 5 && u != from.len() - 1 {
            to.push(' ');
        }
    }
    to
}

/// "Decipher" with the Atbash cipher.
pub fn decode(cipher: &str) -> String {
    cipher.chars().filter(|c| c.is_alphanumeric()).map(|c| 
        if c.is_alphabetic() {((25u8 - (c as u8 - 'a' as u8))%26 + 'a' as u8) as char} else {c}
    ).collect()
}
