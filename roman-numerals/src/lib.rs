use std::fmt::{Display, Formatter, Result};

pub struct Roman {
    numeral: String,
}

impl Display for Roman {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        f.write_str(&self.numeral)
    }
}

impl From<u32> for Roman {
    fn from(num: u32) -> Self {
        assert!(num > 0 && num < 4000);

        let thousands = num / 1000;
        let hundreds = (num % 1000) / 100;
        let tens = (num % 100) / 10;
        let ones = num % 10;

        let mut numeral = String::new();
        for _ in 0..thousands {
            numeral.push('M');
        }
        match hundreds {
            9 => {
                numeral.push('C');
                numeral.push('M');
            },
            5 | 6 | 7 | 8 => {
                numeral.push('D');
                for _ in 6..=hundreds {
                    numeral.push('C');
                }
            },
            4 => {
                numeral.push('C');
                numeral.push('D');

            },
            3 | 2 | 1 => {
                for _ in 0..hundreds {
                    numeral.push('C');
                }
            },
            _ => ()
        }

        match tens {
            9 => {
                numeral.push('X');
                numeral.push('C');
            },
            5 | 6 | 7 | 8 => {
                numeral.push('L');
                for _ in 6..=tens{
                    numeral.push('X');
                }
            },
            4 => {
                numeral.push('X');
                numeral.push('L');

            },
            3 | 2 | 1 => {
                for _ in 0..tens {
                    numeral.push('X');
                }
            },
            _ => ()
        }

        match ones {
            9 => {
                numeral.push('I');
                numeral.push('X');
            },
            5 | 6 | 7 | 8 => {
                numeral.push('V');
                for _ in 6..=ones{
                    numeral.push('I');
                }
            },
            4 => {
                numeral.push('I');
                numeral.push('V');

            },
            3 | 2 | 1 => {
                for _ in 0..ones {
                    numeral.push('I');
                }
            },
            _ => ()
        }


        Self {
            numeral,
        }
    }
}
