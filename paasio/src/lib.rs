use std::io::{Read, Result, Write};

// the PhantomData instances in this file are just to stop compiler complaints
// about missing generics; feel free to remove them

pub struct ReadStats<R> {
    data: R,
    read_bytes: usize,
    read_operations: usize,
}

impl<R: Read> ReadStats<R> {
    // _wrapped is ignored because R is not bounded on Debug or Display and therefore
    // can't be passed through format!(). For actual implementation you will likely
    // wish to remove the leading underscore so the variable is not ignored.
    pub fn new(wrapped: R) -> ReadStats<R> {
        ReadStats {
            data: wrapped,
            read_bytes: 0usize,
            read_operations: 0usize,
        }
    }

    pub fn get_ref(&self) -> &R {
        &self.data
    }

    pub fn bytes_through(&self) -> usize {
        self.read_bytes
    }

    pub fn reads(&self) -> usize {
        self.read_operations
    }
}

impl<R: Read> Read for ReadStats<R> {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize> {
        let result = self.data.read(buf);
        match result {
            Ok(number_of_bytes) => {
                self.read_bytes += number_of_bytes;
                self.read_operations += 1;
                result
            }
            _ => result,
        }
    }
}

pub struct WriteStats<W> {
    data: W,
    written_bytes: usize,
    write_operations: usize,
}

impl<W: Write> WriteStats<W> {
    // _wrapped is ignored because W is not bounded on Debug or Display and therefore
    // can't be passed through format!(). For actual implementation you will likely
    // wish to remove the leading underscore so the variable is not ignored.
    pub fn new(wrapped: W) -> WriteStats<W> {
        WriteStats {
            data: wrapped,
            written_bytes: 0usize,
            write_operations: 0usize,
        }
    }

    pub fn get_ref(&self) -> &W {
        &self.data
    }

    pub fn bytes_through(&self) -> usize {
        self.written_bytes
    }

    pub fn writes(&self) -> usize {
        self.write_operations
    }
}

impl<W: Write> Write for WriteStats<W> {
    fn write(&mut self, buf: &[u8]) -> Result<usize> {
        let result = self.data.write(buf);
        match result {
            Ok(number_of_bytes) => {
                self.written_bytes += number_of_bytes;
                self.write_operations += 1;
                result
            }
            _ => result,
        }
    }

    fn flush(&mut self) -> Result<()> {
        self.data.flush()
    }
}
