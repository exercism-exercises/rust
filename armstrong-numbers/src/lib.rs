pub fn is_armstrong_number(num: u32) -> bool {
    const RADIX:u32 = 10;
    let value = num.to_string();
    let digits = value.chars();
    let num_of_digits:u32 = digits.clone().count().try_into().unwrap(); 

    let mut new_num:u32 = 0;

    for d in digits {
        new_num = new_num.saturating_add(u32::saturating_pow(d.to_digit(RADIX).unwrap(), num_of_digits))
    }

    num == new_num
}
