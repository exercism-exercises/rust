use ndarray::Array2;

/**
 * 
 * Idea:
 * 0. we create a 2d array of chars arr[[i, j]]
 * 1. on an iterator S we only store (i, j) where arr[[i, j]] == '+'
 * as all vertices are denoted by '+'
 * 2. for all s1 in S we compare it with s2 in S so that s1 < s2 coordinate-wise
 * and check whether it forms a rectangle 
 * 3. if true count it!
 */

/// 0.
fn get_grid(lines: &[&str]) -> Array2<char> {
    let height = lines.len();
    let width =lines.get(0).map_or(0, |s| s.len());

    let flat_vec: Vec<char> = lines
        .iter()
        .flat_map(|line| line.chars())
        .collect();

    Array2::from_shape_vec((height, width), flat_vec)
        .expect("Failed to create Array2")
}

fn is_vertex(ch: char) -> bool {
    ch == '+'
}

fn is_vertical_side(ch: char) -> bool {
    ch == '|'
}

fn is_horizontal_side(ch: char) -> bool {
    ch == '-'
}

fn get_vertices(grid: &Array2<char>) -> Vec<(usize, usize)> {
    let mut vertices: Vec<(usize, usize)> = Vec::new();
    for i in 0..grid.dim().0 {
        for j in 0..grid.dim().1 {
            if is_vertex(grid[[i, j]]) {
                vertices.push((i, j));
            }
        }
    }
    vertices
}

// Check if a rectangle exists with top-left corner at (i, j) and bottom-right corner at (k, l)
fn is_rectangle(grid: &Array2<char>, i1: usize, j1: usize, i2: usize, j2: usize) -> bool {
    let check_vertices = is_vertex(grid[[i1, j2]]) && is_vertex(grid[[i2, j1]]);
    let check_upper_horizontal_side = (j1+1..j2).all(|j| is_vertex(grid[[i1, j]]) || is_horizontal_side(grid[[i1, j]]));
    let check_lower_horizontal_side = (j1+1..j2).all(|j| is_vertex(grid[[i2, j]]) || is_horizontal_side(grid[[i2, j]]));
    let check_left_vertical_side = (i1+1..i2).all(|i| is_vertex(grid[[i, j1]]) || is_vertical_side(grid[[i, j1]]));
    let check_right_vertical_side = (i1+1..i2).all(|i| is_vertex(grid[[i, j2]]) || is_vertical_side(grid[[i, j2]]));

    check_vertices && check_upper_horizontal_side && check_lower_horizontal_side && check_left_vertical_side && check_right_vertical_side
}

pub fn count(lines: &[&str]) -> u32 {
    if lines.is_empty() || lines[0].is_empty() {
        return 0;
    }

    let grid: Array2<char> = get_grid(lines);
    let mut vertices = get_vertices(&grid);
    let mut count = 0;
    vertices.sort_unstable_by(|v1, v2| (v2.0 + v2.1).cmp(&(v1.0 + v1.1)));
    while !vertices.is_empty() {
        let (i1, j1) = vertices.pop().unwrap();
        for (i2, j2) in vertices.iter().cloned().filter(|&(i2, j2)| i2 > i1 && j2 > j1) {
           if is_rectangle(&grid, i1, j1, i2, j2) {
            count += 1;
           }
        }
    }

    count
}
