use rand::distributions::Distribution;
use rand::distributions::Uniform;

fn modular_pow(n: u64, e: u64, p: u64) -> u64 {
    let mut num = n % p;
    let mut result: u64 = 1;
    let mut exp: u64 = e % (p - 1);
    while exp > 0 {
        let parity = exp % 2;
        if parity == 1 {
            result *= num;
            result %= p;
            exp -= 1;
        } else {
            num *= num % p;
            num %= p;
            exp /= 2;
        }
    }
    result % p
}

pub fn private_key(p: u64) -> u64 {
    let mut rng = rand::thread_rng();
    let die = Uniform::from(2u64..p);
    die.sample(&mut rng)
}

pub fn public_key(p: u64, g: u64, a: u64) -> u64 {
    //A = gᵃ mod p
    modular_pow(g, a, p)
}

pub fn secret(p: u64, b_pub: u64, a: u64) -> u64 {
    modular_pow(b_pub, a, p)
}
