use std::cmp::Eq;
use std::collections::HashMap;
use std::hash::Hash;

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct CustomSet<T: Hash + PartialEq + Eq + Clone> {
    map: HashMap<T, bool>,
}

impl<T: Hash + PartialEq + Eq + Clone> CustomSet<T> {
    pub fn new(_input: &[T]) -> Self {
        let mut map = HashMap::new();
        for k in _input {
            map.insert(k.clone(), true);
        }
        Self {
            map: map,
        }
    }

    pub fn contains(&self, _element: &T) -> bool {
        self.map.contains_key(_element)
    }

    pub fn add(&mut self, _element: T) {
        self.map.insert(_element, true);
    }

    pub fn is_subset(&self, _other: &Self) -> bool {
        self.map.keys().all(|k| _other.contains(k))
    }

    pub fn is_empty(&self) -> bool {
        self.map.is_empty()
    }

    pub fn is_disjoint(&self, _other: &Self) -> bool {
        self.map.keys().all(|k| !_other.contains(k))
    }

    #[must_use]
    pub fn intersection(&self, _other: &Self) -> Self {
        Self {
            map: self
                .map
                .keys()
                .filter(|&k| _other.contains(k))
                .cloned()
                .map(|k| (k, true))
                .collect::<HashMap<T, bool>>(),
        }
    }

    #[must_use]
    pub fn difference(&self, _other: &Self) -> Self {
        Self {
            map: self
                .map
                .keys()
                .cloned()
                .filter(|k| !_other.contains(k))
                .map(|k| (k, true))
                .collect::<HashMap<T, bool>>(),
        }
    }

    #[must_use]
    pub fn union(&self, _other: &Self) -> Self {
        let mut map = self.map.clone();
        for (k, v) in _other.map.iter() {
            map.insert(k.clone(), *v);
        }
        Self { map: map }
    }
}
