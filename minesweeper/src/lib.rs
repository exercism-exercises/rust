use std::collections::HashSet;

const ASTERISK: u8 = 42u8;
const ZERO: u8 = 48u8;
const SPACE: u8 = 32u8;

/// Takes a slice of &str representation of minesweep board and annotates with numbers
pub fn annotate(minefield: &[&str]) -> Vec<String> {
    // Get a sparse representation of mines
    let tuples: HashSet<(usize, usize)> = HashSet::from_iter(
        minefield
            .iter()
            .enumerate()
            .map(|(i, row)| {
                row.as_bytes()
                    .iter()
                    .enumerate()
                    .filter(|&(_, &u)| u == ASTERISK)
                    .map(move |(j, _)| (i, j))
            })
            .flatten(),
    );

    // Set up the output and required constants
    let mut result: Vec<String> = Vec::new();
    let m = minefield.len();
    let n = if m > 0 { minefield[0].len() } else { 0 };

    // Iterate and annotate with number of neighboring mines if a cell doesn't have mine, otherwise mark it as asterisk
    for i in 0..m {
        let mut row = String::new();

        for j in 0..n {
            row.push(check(i, j, m, n, &tuples) as char);
        }

        result.push(row);
    }
    result
}

fn check(i: usize, j: usize, m: usize, n: usize, tuples: &HashSet<(usize, usize)>) -> u8 {
    let tuple: (usize, usize) = (i, j);

    if tuples.contains(&tuple) {
        return ASTERISK;
    }

    let can_go_up = i > 0;
    let can_go_down = i < m - 1;

    let can_go_left = j > 0;
    let can_go_right = j < n - 1;

    let mut number: u8 = 0u8;

    // check column
    if can_go_up && tuples.contains(&(i - 1, j)) {
        number += 1;
    }

    if can_go_down && tuples.contains(&(i + 1, j)) {
        number += 1;
    }

    // check row
    if can_go_left && tuples.contains(&(i, j - 1)) {
        number += 1;
    }

    if can_go_right && tuples.contains(&(i, j + 1)) {
        number += 1;
    }

    // check diagonals
    if can_go_up && can_go_left && tuples.contains(&(i - 1, j - 1)) {
        number += 1;
    }

    if can_go_up && can_go_right && tuples.contains(&(i - 1, j + 1)) {
        number += 1;
    }

    if can_go_down && can_go_left && tuples.contains(&(i + 1, j - 1)) {
        number += 1;
    }

    if can_go_down && can_go_right && tuples.contains(&(i + 1, j + 1)) {
        number += 1;
    }

    // return
    if number > 0u8 {
        number + ZERO
    } else {
        SPACE
    }
}
