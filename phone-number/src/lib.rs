pub fn number(user_number: &str) -> Option<String> {
    //NXX NXX-XXXX
    let trimmed_number: Vec<char> = user_number.chars().filter(|c| c.is_ascii_digit()).collect();
    let n: usize = trimmed_number.len();
    match n {
        10 if trimmed_number[0] >= '2' && trimmed_number[3] >= '2' => {
            Some(trimmed_number.iter().collect())
        }
        11 if trimmed_number[0] == '1' && trimmed_number[1] >= '2' && trimmed_number[4] >= '2' => {
            Some(
                trimmed_number
                    .iter()
                    .enumerate()
                    .filter_map(|(u, c)| if u > 0 { Some(c) } else { None })
                    .collect::<String>(),
            )
        }
        _ => None,
    }
}
