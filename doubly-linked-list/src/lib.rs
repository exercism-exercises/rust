// this module adds some functionality based on the required implementations
// here like: `LinkedList::pop_back` or `Clone for LinkedList<T>`
// You are free to use anything in it, but it's mainly for the test framework.
mod pre_implemented;

use std::marker::PhantomData;
use std::ptr::NonNull;

type Link<T> = Option<NonNull<Node<T>>>;
struct Node<T> {
    front: Link<T>,
    back: Link<T>,
    elem: T,
}
pub struct LinkedList<T> {
    front: Link<T>,
    back: Link<T>,
    len: usize,
    _boo: PhantomData<T>,
}

pub struct Cursor<'a, T> {
    list: &'a mut LinkedList<T>,
    cur: Link<T>,
    index: Option<usize>,
}

pub struct Iter<'a, T> {
    front: Link<T>,
    len: usize,
    _boo: PhantomData<&'a T>,
}

impl<T> LinkedList<T> {
    pub fn new() -> Self {
        Self {
            front: None,
            back: None,
            len: 0,
            _boo: PhantomData,
        }
    }

    // You may be wondering why it's necessary to have is_empty()
    // when it can easily be determined from len().
    // It's good custom to have both because len() can be expensive for some types,
    // whereas is_empty() is almost always cheap.
    // (Also ask yourself whether len() is expensive for LinkedList)
    pub fn is_empty(&self) -> bool {
        self.len == 0
    }

    pub fn len(&self) -> usize {
        self.len
    }

    /// Return a cursor positioned on the front element
    pub fn cursor_front(&mut self) -> Cursor<'_, T> {
        let index = if self.is_empty() { None } else { Some(0usize) };
        Cursor {
            cur: self.front,
            list: self,
            index,
        }
    }

    /// Return a cursor positioned on the back element
    pub fn cursor_back(&mut self) -> Cursor<'_, T> {
        let index = if self.is_empty() {
            None
        } else {
            Some(self.len())
        };

        Cursor {
            cur: self.back,
            list: self,
            index,
        }
    }

    /// Return an iterator that moves from front to back
    pub fn iter(&self) -> Iter<T> {
        Iter {
            front: self.front,
            len: self.len,
            _boo: PhantomData,
        }
    }
}

impl<T> Drop for LinkedList<T> {
    fn drop(&mut self) {
        let mut cursor = self.cursor_front();
        while cursor.take().is_some() {}
    }
}

impl<T> Default for LinkedList<T> {
    fn default() -> Self {
        Self::new()
    }
}

unsafe impl<T: Send> Send for LinkedList<T> {}
unsafe impl<T: Sync> Sync for LinkedList<T> {}

// the cursor is expected to act as if it is at the position of an element
// and it also has to work with and be able to insert into an empty list.
impl<T> Cursor<'_, T> {
    /// Take a mutable reference to the current element
    pub fn peek_mut(&mut self) -> Option<&mut T> {
        unsafe { Some(&mut (*self.cur?.as_ptr()).elem) }
    }

    /// Move one position forward (towards the back) and
    /// return a reference to the new position
    #[allow(clippy::should_implement_trait)]
    pub fn next(&mut self) -> Option<&mut T> {
        self.move_next();
        self.peek_mut()
    }

    fn move_next(&mut self) {
        if let Some(cur) = self.cur {
            unsafe {
                // We're on a real element, go to its next (back)
                self.cur = (*cur.as_ptr()).back;
                if self.cur.is_some() {
                    *self.index.as_mut().unwrap() += 1;
                } else {
                    // We just walked to the ghost, no more index
                    self.index = None;
                }
            }
        } else if !self.list.is_empty() {
            // We're at the ghost, and there is a real front, so move to it!
            self.cur = self.list.front;
            self.index = Some(0)
        } else {
            // We're at the ghost, but that's the only element... do nothing.
        }
    }

    /// Move one position backward (towards the front) and
    /// return a reference to the new position
    pub fn prev(&mut self) -> Option<&mut T> {
        self.move_prev();
        self.peek_mut()
    }

    fn move_prev(&mut self) {
        if let Some(cur) = self.cur {
            unsafe {
                // We're on a real element, go to its previous (front)
                self.cur = (*cur.as_ptr()).front;
                if self.cur.is_some() {
                    *self.index.as_mut().unwrap() -= 1;
                } else {
                    // We just walked to the ghost, no more index
                    self.index = None;
                }
            }
        } else if !self.list.is_empty() {
            // We're at the ghost, and there is a real back, so move to it!
            self.cur = self.list.back;
            self.index = Some(self.list.len - 1)
        } else {
            // We're at the ghost, but that's the only element... do nothing.
        }
    }

    /// Remove and return the element at the current position and move the cursor
    /// to the neighboring element that's closest to the back. This can be
    /// either the next or previous position.
    pub fn take(&mut self) -> Option<T> {
        match self.cur {
            Some(mut cur) => unsafe {
                self.list.len -= 1;

                let front = cur.as_mut().front;

                let back = cur.as_mut().back;

                self.cur = None;

                match front {
                    Some(mut front) => {
                        front.as_mut().back = back;

                        self.cur = Some(front);
                    }

                    _ => self.list.front = back,
                }

                match back {
                    Some(mut back) => {
                        back.as_mut().front = front;

                        self.cur = Some(back);
                    }

                    _ => self.list.back = front,
                }

                Some(Box::from_raw(cur.as_ptr()).elem)
            },

            _ => None,
        }
    }

    pub fn insert_after(&mut self, element: T) {
        unsafe {
            let new = NonNull::new_unchecked(Box::into_raw(Box::new(Node {
                back: None,
                front: None,
                elem: element,
            })));
            if let Some(old) = self.cur {
                // Put the new back before the old one
                (*new.as_ptr()).front = Some(old);
                (*new.as_ptr()).back = (*old.as_ptr()).back;
                (*old.as_ptr()).back = Some(new);
                match (*new.as_ptr()).back {
                    Some(right) => (*right.as_ptr()).front = Some(new),
                    None => self.list.back = Some(new),
                }
                
            } else {
                // If there's no back, then we're the empty list and need
                // to set the front too.
                self.list.front = Some(new);
                self.list.back = Some(new);
            }
            // These things always happen!
            
            self.list.len += 1;
        }
    }

    pub fn insert_before(&mut self, element: T) {
        unsafe {
            let new = NonNull::new_unchecked(Box::into_raw(Box::new(Node {
                front: None,
                back: None,
                elem: element,
            })));
            if let Some(old) = self.cur {
                // Put the new back before the old one
                (*new.as_ptr()).back = Some(old);
                (*new.as_ptr()).front = (*old.as_ptr()).front;
                (*old.as_ptr()).front = Some(new);
                match (*new.as_ptr()).front {
                    Some(left) => (*left.as_ptr()).back = Some(new),
                    None => self.list.front = Some(new),
                }
                
            } else {
                // If there's no back, then we're the empty list and need
                // to set the front too.
                self.list.front = Some(new);
                self.list.back = Some(new);
            }
            self.list.len += 1;
        }
    }
}

impl<'a, T> Iterator for Iter<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        // While self.front == self.back is a tempting condition to check here,
        // it won't do the right for yielding the last element! That sort of
        // thing only works for arrays because of "one-past-the-end" pointers.
        if self.len > 0 {
            // We could unwrap front, but this is safer and easier
            self.front.map(|node| unsafe {
                self.len -= 1;
                self.front = (*node.as_ptr()).back;
                &(*node.as_ptr()).elem
            })
        } else {
            None
        }
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.len, Some(self.len))
    }
}
