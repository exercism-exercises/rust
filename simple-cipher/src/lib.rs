use std::iter::{repeat_with, zip};

use rand::Rng;

const A: u8 = b'a';
const CHARSET: &[u8] = b"abcdefghijklmnopqrstuvwxyz";

pub fn encode(key: &str, s: &str) -> Option<String> {
    if key.is_empty() {
        None
    } else {
        zip(key.chars().cycle(), s.chars())
            .map(|(k, l)| {
                if !k.is_ascii_lowercase() {
                    None
                } else {
                    let shift: u8 = k.to_ascii_lowercase() as u8 - A;
                    let letter: u8 = l.to_ascii_lowercase() as u8;
                    let c = (((letter - A) + shift) % 26 + A) as char;
                    Some(c)
                }
            })
            .collect::<Option<String>>()
    }
}

pub fn decode(key: &str, s: &str) -> Option<String> {
    if key.is_empty() {
        None
    } else {
        zip(key.chars().cycle(), s.chars())
            .map(|(k, l)| {
                if !k.is_ascii_lowercase() {
                    None
                } else {
                    let shift: u8 = k.to_ascii_lowercase() as u8 - A;
                    let letter: u8 = l.to_ascii_lowercase() as u8;
                    let c = ((26 - shift + (letter - A)) % 26 + A) as char;
                    Some(c)
                }
            })
            .collect::<Option<String>>()
    }
}

pub fn encode_random(s: &str) -> (String, String) {
    let mut rng = rand::thread_rng();
    let one_char = || CHARSET[rng.gen_range(0..CHARSET.len())] as char;
    let key: String = repeat_with(one_char).take(101).collect();
    let encoded = encode(key.as_str(), s);
    (key, encoded.unwrap())
}
