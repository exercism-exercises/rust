use std::collections::HashMap;

pub struct CircularBuffer<T> {
    buffer: HashMap<usize, T>,
    capacity: usize,
    start: usize,
    curr: usize,
}

#[derive(Debug, PartialEq, Eq)]
pub enum Error {
    EmptyBuffer,
    FullBuffer,
}

impl<T> CircularBuffer<T> {
    pub fn new(capacity: usize) -> Self {
        Self {
            buffer: HashMap::new(),
            capacity,
            start: 0,
            curr: 0,
        }
    }

    fn is_full(&self) -> bool {
        self.buffer.len() == self.capacity
    }

    pub fn write(&mut self, element: T) -> Result<(), Error> {
        if self.is_full() {
            Err(Error::FullBuffer)
        } else {
            self.buffer.insert(self.curr, element);
            self.curr += 1;
            self.curr %= self.capacity;
            Ok(())
        }
    }

    pub fn read(&mut self) -> Result<T, Error> {
        if self.buffer.is_empty() {
            Err(Error::EmptyBuffer)
        } else {
            let value = self.buffer.remove(&self.start).unwrap();
            self.start += 1;
            self.start %= self.capacity;
            Ok(value)
        }
    }

    pub fn clear(&mut self) {
        self.buffer.clear();
        self.curr = 0;
        self.start = 0;
    }

    pub fn overwrite(&mut self, element: T) {
        if !self.is_full() {
            self.write(element).unwrap();
        } else {
            let _previous_value = self.buffer.remove(&self.start).unwrap();
            self.buffer.insert(self.start, element);
            self.start += 1;
            self.start %= self.capacity;
        }
    }
}
