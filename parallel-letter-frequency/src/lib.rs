use rayon::prelude::*;
use std::collections::HashMap;

fn merge_frequencies(mut a: HashMap<char, usize>, b: HashMap<char, usize>) -> HashMap<char, usize> {
    for (word, count) in b {
        *a.entry(word).or_insert(0) += count
    }
    a
}

pub fn frequency(input: &[&str], worker_count: usize) -> HashMap<char, usize> {
    // this hashmap will be our result
    let n = input.len();

    // if of zero length, skip it
    if n == 0usize {
        return HashMap::new();
    }

    // (input.len() + worker_count - 1) / worker_count;
    // tasks / threads + (i < tasks % threads)
    let chunk_size = n / worker_count + if n % worker_count > 0 { 1 } else { 0 };
    input
        .into_par_iter()
        .chunks(chunk_size)
        .into_par_iter()
        .map(|chunk| {
            let mut chunk_letter_to_count: HashMap<char, usize> = HashMap::new();

            for c in chunk.iter().map(|word| word.chars()).flatten() {
                if c.is_alphabetic() {
                    *chunk_letter_to_count
                        .entry(c.to_ascii_lowercase())
                        .or_insert(0) += 1;
                }
            }

            chunk_letter_to_count
        })
        .reduce_with(merge_frequencies)
        .unwrap()
}
