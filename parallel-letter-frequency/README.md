# Parallel Letter Frequency

Welcome to Parallel Letter Frequency on Exercism's Rust Track.
If you need help running the tests or submitting your code, check out `HELP.md`.

## Instructions

Count the frequency of letters in texts using parallel computation.

Parallelism is about doing things in parallel that can also be done sequentially.
A common example is counting the frequency of letters.
Create a function that returns the total frequency of each letter in a list of texts and that employs parallelism.

## Parallel Letter Frequency in Rust

Learn more about concurrency in Rust here:

- [Concurrency](https://doc.rust-lang.org/book/ch16-00-concurrency.html)

## Bonus

This exercise also includes a benchmark, with a sequential implementation as a
baseline. You can compare your solution to the benchmark. Observe the
effect different size inputs have on the performance of each. Can you
surpass the benchmark using concurrent programming techniques?

As of this writing, test::Bencher is unstable and only available on
*nightly* Rust. Run the benchmarks with Cargo:

```
cargo bench
```

If you are using rustup.rs:

```
rustup run nightly cargo bench
```

- [Benchmark tests](https://doc.rust-lang.org/stable/unstable-book/library-features/test.html)

Learn more about nightly Rust:

- [Nightly Rust](https://doc.rust-lang.org/book/appendix-07-nightly-rust.html)
- [Installing Rust nightly](https://rust-lang.github.io/rustup/concepts/channels.html#working-with-nightly-rust)

## Comment

rayon
```
󰣛 dan-seol  …/exercism-exercises/rust/parallel-letter-frequency   v1.76.0   20:03  
 rustup run nightly cargo bench
   Compiling crossbeam-utils v0.8.19
   Compiling rayon-core v1.12.1
   Compiling either v1.11.0
   Compiling crossbeam-epoch v0.9.18
   Compiling crossbeam-deque v0.8.5
   Compiling rayon v1.10.0
   Compiling parallel-letter-frequency v0.0.0 (/home/dan-seol/exercism-exercises/rust/parallel-letter-frequency)
    Finished `bench` profile [optimized] target(s) in 2.58s
     Running unittests src/lib.rs (target/release/deps/parallel_letter_frequency-c3ad857599d276ce)

running 0 tests

test result: ok. 0 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out; finished in 0.00s

     Running benches/benchmark.rs (target/release/deps/benchmark-63df81f534d9b29d)

running 6 tests
test bench_large_parallel   ... bench:     170,273 ns/iter (+/- 44,615)
test bench_large_sequential ... bench:     446,353 ns/iter (+/- 4,566)
test bench_small_parallel   ... bench:      28,706 ns/iter (+/- 929)
test bench_small_sequential ... bench:      15,513 ns/iter (+/- 177)
test bench_tiny_parallel    ... bench:         114 ns/iter (+/- 2)
test bench_tiny_sequential  ... bench:          56 ns/iter (+/- 0)

test result: ok. 0 passed; 0 failed; 0 ignored; 6 measured; 0 filtered out; finished in 17.00s
```
non rayon
```

󰣛 dan-seol  …/exercism-exercises/rust/parallel-letter-frequency   v1.76.0   20:03  
 rustup run nightly cargo bench
   Compiling parallel-letter-frequency v0.0.0 (/home/dan-seol/exercism-exercises/rust/parallel-letter-frequency)
    Finished `bench` profile [optimized] target(s) in 0.41s
     Running unittests src/lib.rs (target/release/deps/parallel_letter_frequency-c3ad857599d276ce)

running 0 tests

test result: ok. 0 passed; 0 failed; 0 ignored; 0 measured; 0 filtered out; finished in 0.00s

     Running benches/benchmark.rs (target/release/deps/benchmark-63df81f534d9b29d)

running 6 tests
test bench_large_parallel   ... bench:     205,994 ns/iter (+/- 65,480)
test bench_large_sequential ... bench:     442,561 ns/iter (+/- 5,078)
test bench_small_parallel   ... bench:      83,046 ns/iter (+/- 9,561)
test bench_small_sequential ... bench:      15,192 ns/iter (+/- 312)
test bench_tiny_parallel    ... bench:      39,877 ns/iter (+/- 6,317)
test bench_tiny_sequential  ... bench:          53 ns/iter (+/- 0)

test result: ok. 0 passed; 0 failed; 0 ignored; 6 measured; 0 filtered out; finished in 17.77s

```

## Source

### Created by

- @EduardoBautista

### Contributed to by

- @andrewclarkson
- @ashleygwilliams
- @ccouzens
- @ClashTheBunny
- @coriolinus
- @cwhakes
- @EduardoBautista
- @efx
- @ErikSchierboom
- @etrepum
- @glennpratt
- @IanWhitney
- @kytrinyx
- @lutostag
- @mkantor
- @nfiles
- @petertseng
- @rofrol
- @sjwarner-bp
- @stringparser
- @xakon
- @ZapAnton