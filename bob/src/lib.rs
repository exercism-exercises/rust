const QUESTION_MARK: char = '?';

pub fn reply(message: &str) -> &str {
    let message = message.trim();
    if message.is_empty() {
        return "Fine. Be that way!";
    }

    let is_question = message.ends_with(QUESTION_MARK);
    let is_all_caps =
        message.chars().any(|c| c.is_alphabetic()) && message.to_uppercase().as_str() == message;
    match message {
        _m if is_all_caps && is_question => "Calm down, I know what I'm doing!",
        _m if is_all_caps => "Whoa, chill out!",
        _m if is_question => "Sure.",
        _ => "Whatever.",
    }
}
