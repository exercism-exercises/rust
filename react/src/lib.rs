use std::collections::HashMap;
use uuid::Uuid;

/// `InputCellId` is a unique identifier for an input cell.
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub struct InputCellId(Uuid);
/// `ComputeCellId` is a unique identifier for a compute cell.
/// Values of type `InputCellId` and `ComputeCellId` should not be mutually assignable,
/// demonstrated by the following tests:
///
/// ```compile_fail
/// let mut r = react::Reactor::new();
/// let input: react::ComputeCellId = r.create_input(111);
/// ```
///
/// ```compile_fail
/// let mut r = react::Reactor::new();
/// let input = r.create_input(111);
/// let compute: react::InputCellId = r.create_compute(&[react::CellId::Input(input)], |_| 222).unwrap();
/// ```
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub struct ComputeCellId(Uuid);

struct ComputeCell<'a, T> {
    dependencies: Vec<CellId>,
    compute_func: Box<dyn 'a + Fn(&[T]) -> T>,
    callbacks: HashMap<CallbackId, Box<dyn 'a + FnMut(T)>>,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub struct CallbackId(Uuid);

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum CellId {
    Input(InputCellId),
    Compute(ComputeCellId),
}

#[derive(Debug, PartialEq, Eq)]
pub enum RemoveCallbackError {
    NonexistentCell,
    NonexistentCallback,
}

pub struct Reactor<'a, T> {
    // Just so that the compiler doesn't complain about an unused type parameter.
    // You probably want to delete this field.
    inputs: HashMap<InputCellId, T>,
    computations: HashMap<ComputeCellId, ComputeCell<'a, T>>,
}

// You are guaranteed that Reactor will only be tested against types that are Copy + PartialEq.
impl<'a, T: Copy + PartialEq> Reactor<'a, T> {
    pub fn new() -> Self {
        Self {
            inputs: HashMap::new(),
            computations: HashMap::new(),
        }
    }

    // Creates an input cell with the specified initial value, returning its ID.
    pub fn create_input(&mut self, initial: T) -> InputCellId {
        let input_cell_id: InputCellId = InputCellId(Uuid::new_v4());
        self.inputs.insert(input_cell_id, initial);
        input_cell_id
    }

    // Creates a compute cell with the specified dependencies and compute function.
    // The compute function is expected to take in its arguments in the same order as specified in
    // `dependencies`.
    // You do not need to reject compute functions that expect more arguments than there are
    // dependencies (how would you check for this, anyway?).
    //
    // If any dependency doesn't exist, returns an Err with that nonexistent dependency.
    // (If multiple dependencies do not exist, exactly which one is returned is not defined and
    // will not be tested)
    //
    // Notice that there is no way to *remove* a cell.
    // This means that you may assume, without checking, that if the dependencies exist at creation
    // time they will continue to exist as long as the Reactor exists.
    pub fn create_compute<F: Fn(&[T]) -> T + 'a>(
        &mut self,
        dependencies: &[CellId],
        compute_func: F,
    ) -> Result<ComputeCellId, CellId> {
        for &dependency in dependencies.iter() {
            let does_dependency_exist = match dependency {
                CellId::Input(input_cell_id) => self.inputs.contains_key(&input_cell_id),
                CellId::Compute(compute_cell_id) => self.computations.contains_key(&compute_cell_id)
            };

            if !does_dependency_exist {
                return Err(dependency);
            }
        }
        let compute_cell_id: ComputeCellId = ComputeCellId(Uuid::new_v4());
        let dependencies = dependencies.to_vec();
        let compute_cell: ComputeCell<'a, T> = ComputeCell {
            dependencies,
            compute_func: Box::new(compute_func),
            callbacks: HashMap::new(),
        };
        self.computations.insert(compute_cell_id, compute_cell);
        Ok(compute_cell_id)
    }

    // Retrieves the current value of the cell, or None if the cell does not exist.
    //
    // You may wonder whether it is possible to implement `get(&self, id: CellId) -> Option<&Cell>`
    // and have a `value(&self)` method on `Cell`.
    //
    // It turns out this introduces a significant amount of extra complexity to this exercise.
    // We chose not to cover this here, since this exercise is probably enough work as-is.
    pub fn value(&self, id: CellId) -> Option<T> {
        match id {
            CellId::Input(input_cell_id) => self.inputs.get(&input_cell_id).copied(),
            CellId::Compute(compute_cell_id) => {
                if let Some(&ref compute_cell) = self.computations.get(&compute_cell_id) {
                    let dependencies: Vec<Option<T>> = compute_cell.dependencies.iter().map(|&cell_id| self.value(cell_id)).collect();
    
                    if dependencies.iter().all(|&dependency| dependency.is_some()) {
                        let resolved_dependencies: Vec<T> = dependencies.iter().filter_map(|&dependency| dependency).collect();
                        let resolved_value = (*compute_cell.compute_func)(&resolved_dependencies);
                        return Some(resolved_value);
                    }
                }
                None
            }
        }
    }
    // Sets the value of the specified input cell.
    //
    // Returns false if the cell does not exist.
    //
    // Similarly, you may wonder about `get_mut(&mut self, id: CellId) -> Option<&mut Cell>`, with
    // a `set_value(&mut self, new_value: T)` method on `Cell`.
    //
    // As before, that turned out to add too much extra complexity.
    pub fn set_value(&mut self, id: InputCellId, new_value: T) -> bool {
        if let std::collections::hash_map::Entry::Occupied(mut _e) = self.inputs.entry(id) {
            let values_before: HashMap<ComputeCellId, T> = self.computations
            .keys().into_iter().map(|&compute_cell_id| (compute_cell_id, self.value(CellId::Compute(compute_cell_id)).unwrap())).collect();
            let did_insert = self.inputs.insert(id, new_value).is_some();
            let values_after: HashMap<ComputeCellId, T> = self.computations
            .keys().into_iter().map(|&compute_cell_id| (compute_cell_id, self.value(CellId::Compute(compute_cell_id)).unwrap())).collect();

            for (compute_cell_id, compute_cell) in self.computations.iter_mut() {
                let value_before = values_before[compute_cell_id];
                let value_after = values_after[compute_cell_id];
                if value_before != value_after {
                    for callback in compute_cell.callbacks.values_mut() {
                        callback(value_after);
                    }
                }
            }
            return did_insert;
        }
        false
    }

    // Adds a callback to the specified compute cell.
    //
    // Returns the ID of the just-added callback, or None if the cell doesn't exist.
    //
    // Callbacks on input cells will not be tested.
    //
    // The semantics of callbacks (as will be tested):
    // For a single set_value call, each compute cell's callbacks should each be called:
    // * Zero times if the compute cell's value did not change as a result of the set_value call.
    // * Exactly once if the compute cell's value changed as a result of the set_value call.
    //   The value passed to the callback should be the final value of the compute cell after the
    //   set_value call.
    pub fn add_callback<F: FnMut(T) + 'a>(
        &mut self,
        id: ComputeCellId,
        callback: F,
    ) -> Option<CallbackId> {
        if let Some(compute_cell) = self.computations.get_mut(&id) {
            let callback_id: CallbackId = CallbackId(Uuid::new_v4());
            compute_cell.callbacks.insert(callback_id, Box::new(callback));
            Some(callback_id)
        } else {
            None
        }
        
    }

    // Removes the specified callback, using an ID returned from add_callback.
    //
    // Returns an Err if either the cell or callback does not exist.
    //
    // A removed callback should no longer be called.
    pub fn remove_callback(
        &mut self,
        cell: ComputeCellId,
        callback: CallbackId,
    ) -> Result<(), RemoveCallbackError> {
        if let Some(compute_cell) = self.computations.get_mut(&cell) {
            if compute_cell.callbacks.remove(&callback).is_some() {
                Ok(())
            } else {
                Err(RemoveCallbackError::NonexistentCallback)
            }
        } else {
            Err(RemoveCallbackError::NonexistentCell)
        }
    }
}
