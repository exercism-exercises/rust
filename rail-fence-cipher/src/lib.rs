use std::{arch::x86_64::CpuidResult, ops::SubAssign};

pub struct RailFence {
    rails: usize,
}


impl RailFence {

    fn get_offset(&self, rail: usize) -> usize {
        if rail == 0 || rail == self.rails - 1 {
            (2*self.rails).saturating_sub(2)
        } else {
            (2*self.rails).saturating_sub(2).saturating_sub(2*rail)
        }
    }

    pub fn new(rails: u32) -> RailFence {
        Self {
            rails: rails as usize,
        }
    }

    pub fn encode(&self, text: &str) -> String {
        let mut rails: Vec<String> = (0..self.rails).map(|_| String::new()).collect();
        let mut direction: bool = false;
        let mut rail: usize = 0;
        for (u, c) in text.char_indices() {
            rails[rail].push(c);
            let modulus = u % (2 * self.rails - 2);
            if modulus == self.rails - 1 || modulus == 0 {
                direction = !direction;
            }
            if direction {
                rail += 1;
            } else {
                rail -= 1;
            }
        }
        let mut result = String::new();
        for row in rails {
            result.push_str(&row);
        }
        result
    }

    pub fn decode(&self, cipher: &str) -> String {
        let n = cipher.len();
        let phrase: Vec<char> = cipher.chars().collect();
        let mut rails: Vec<Vec<char>> = (0..self.rails).map(|_| (0..n).map(|_| '.').collect()).collect();
        let mut offset = self.get_offset(0);
        let mut u: usize = 0;
        let mut i: usize = 0;
        while u < n {
            rails[0][u] = phrase[i].clone();
            u += offset;
            i += 1;
        }
        
        for rail in 1..self.rails {
            let mut flip = rail;
            u = rail;
            offset = self.get_offset(flip);
            while u < n {
                if i >= n {
                    break;
                }
                rails[rail][u] = phrase[i].clone();
                u += offset;
                i += 1;
                flip = self.rails - flip - 1;
                offset = self.get_offset(flip);

            }
        }
        let mut result = String::new();
        u = 0;
        i = 0;
        let mut direction: bool = false;
        for _ in 0..n {
            result.push(rails[u][i]);
            if u == self.rails - 1 || u == 0 {
                direction = !direction;
            }
            if direction {
                u += 1;
            } else {
                u -= 1;
            }
            i += 1;
        }

        result
    }
}
