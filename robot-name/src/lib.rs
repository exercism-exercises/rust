#[macro_use]
extern crate lazy_static;

use std::{collections::HashSet, sync::Mutex};

use rand::prelude::IteratorRandom; // 0.8

lazy_static! {
    static ref SET: Mutex<HashSet<String>> = {
        let mut s = HashSet::new();
        Mutex::new(s)
    };    
}
pub struct Robot {
    robot_name: String,
}

fn generate_name() -> String {
    let mut rng = rand::thread_rng();
    let mut set = SET.lock().unwrap();
    let prefix: String = ('A'..='Z').choose_multiple(&mut rng, 2).iter().collect();
    let suffix: String = (0..=9)
        .choose_multiple(&mut rng, 3)
        .iter()
        .map(|&num| char::from_digit(num, 10).unwrap())
        .collect();
    let mut name: String = prefix + suffix.as_str();
    while set.contains(&name) {
        let prefix: String = ('A'..='Z').choose_multiple(&mut rng, 2).iter().collect();
        let suffix: String = (0..=9)
            .choose_multiple(&mut rng, 3)
            .iter()
            .map(|&num| char::from_digit(num, 10).unwrap())
            .collect();
        name = prefix + suffix.as_str(); 
    }
    set.insert(name.clone());
    name
}

impl Robot {
    pub fn new() -> Self {
        Self {
            robot_name: generate_name(),
        }
    }

    pub fn name(&self) -> &str {
        self.robot_name.as_str()
    }

    pub fn reset_name(&mut self) {
        self.robot_name = generate_name();
    }
}
