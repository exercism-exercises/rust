use anyhow::Error;
use std::fs::read_to_string;
// use clap::Parser;

/// While using `&[&str]` to handle flags is convenient for exercise purposes,
/// and resembles the output of [`std::env::args`], in real-world projects it is
/// both more convenient and more idiomatic to contain runtime configuration in
/// a dedicated struct. Therefore, we suggest that you do so in this exercise.
///
/// In the real world, it's common to use crates such as [`clap`] or
/// [`structopt`] to handle argument parsing, and of course doing so is
/// permitted in this exercise as well, though it may be somewhat overkill.
///
/// [`clap`]: https://crates.io/crates/clap
/// [`std::env::args`]: https://doc.rust-lang.org/std/env/fn.args.html
/// [`structopt`]: https://crates.io/crates/structopt
/// - `-n` Prepend the line number and a colon (':') to each line in the output, placing the number after the filename (if present).
/// - `-l` Output only the names of the files that contain at least one matching line.
/// - `-i` Match using a case-insensitive comparison.
/// - `-v` Invert the program -- collect all lines that fail to match.
/// - `-x` Search only for lines where the search string matches the entire line.
/// #[derive(Parser, Debug)]
#[derive(Default)]
pub struct Flags {
    //#[arg(short = 'n', action, required = false)]
    prepend_line_number: bool,

    //#[arg(short = 'l', action, required = false)]
    show_file_name_only: bool,

    //#[arg(short = 'i', action, required = false)]
    case_insensitive: bool,

    //#[arg(short = 'v', action, required = false)]
    fail_to_match: bool,

    //#[arg(short = 'x', action, required = false)]
    show_entire_line_match_only: bool,
}

impl Flags {
    pub fn new(flags: &[&str]) -> Self {
        let mut result = Flags::default();
        for s in flags {
            match *s {
                "-n" => result.prepend_line_number = true,
                "-l" => result.show_file_name_only = true,
                "-i" => result.case_insensitive = true,
                "-v" => result.fail_to_match = true,
                "-x" => result.show_entire_line_match_only = true,
                _ => (),
            };
        }
        result
    }
}

pub fn grep(pattern: &str, flags: &Flags, files: &[&str]) -> Result<Vec<String>, Error> {
    let mut file_path_to_match: Vec<(String, Vec<(usize, String)>)> = Vec::new();
    for file_path in files.iter() {
        let p = if flags.case_insensitive {
            pattern.to_uppercase()
        } else {
            String::from(pattern)
        };
        let file = read_to_string(file_path)?;
        let lines: Vec<(usize, String)> = file
            .lines()
            .map(String::from)
            .enumerate()
            .filter(|(_, line)| {
                let line_to_match = if flags.case_insensitive {
                    line.clone().to_uppercase()
                } else {
                    line.clone()
                };
                let does_match = if flags.show_entire_line_match_only {
                    line_to_match == p
                } else {
                    line_to_match.contains(p.as_str())
                };

                if flags.fail_to_match {
                    !does_match
                } else {
                    does_match
                }
            })
            .collect();
        if !lines.is_empty() {
            file_path_to_match.push((file_path.to_string(), lines));
        }
    }

    let n = files.len();

    let result: Vec<String> = file_path_to_match
        .into_iter()
        .flat_map(move |(file_path, matches)| {
            if flags.show_file_name_only {
                vec![file_path]
            } else {
                match (flags.prepend_line_number, n) {
                    (true, 1) => matches
                        .into_iter()
                        .map(|(line_number, line)| format!("{0}:{1}", line_number + 1, line))
                        .collect::<Vec<String>>(),
                    (false, 1) => matches
                        .into_iter()
                        .map(|(_, line)| line)
                        .collect::<Vec<String>>(),
                    (true, _) => matches
                        .into_iter()
                        .map(|(line_number, line)| {
                            format!("{0}:{1}:{2}", file_path, line_number + 1, line)
                        })
                        .collect::<Vec<String>>(),
                    (false, _) => matches
                        .into_iter()
                        .map(|(_, line)| format!("{0}:{1}", file_path, line))
                        .collect::<Vec<String>>(),
                }
            }
        })
        .collect();

    Ok(result)
}
