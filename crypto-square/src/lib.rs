
use itertools::Itertools;


fn normalize(input: &str) -> String {
    input
        .chars()
        .filter(|c| c.is_alphanumeric())
        .map(|c| c.to_ascii_lowercase())
        .collect()
}

fn gather(normalized: String, n: usize) -> String {
    let mut vec: Vec<String> = Vec::new();
    for _ in 0..n {
        vec.push(String::new());
    }
    for (u, c) in normalized.char_indices() {
        let line = vec.get_mut(u % n).unwrap();
        line.push(c);
    }
    let l = normalized.len() % n;
    if l != 0 {
        for u in l..n {
            let line = vec.get_mut(u).unwrap();
            line.push(' ');
        }
    }
    vec.join("")
}

fn reorg(gathered: String, m: usize) -> String {
    gathered.chars().chunks(m).into_iter().map(|chunk| chunk.collect::<String>()).join(" ")
}

pub fn encrypt(input: &str) -> String {
    let normalized: String = normalize(input);
    let mut m: usize = 0;
    while m * (m + 1) < normalized.len() && m * m < normalized.len() {
        m += 1;
    }
    let n = if m * m >= normalized.len() { m } else { m + 1 };
    if n > 0 {
        let gathered = gather(normalized, n);
        reorg(gathered, m)
    } else {
        String::new()
    }

}
