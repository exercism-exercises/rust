fn get_action(u: u8) -> Option<&'static str> {
    match u {
        1 => Some("wink"),
        2 => Some("double blink"),
        4 => Some("close your eyes"),
        8 => Some("jump"),
        _ => None,
    }
}

pub fn actions(n: u8) -> Vec<&'static str> {
    let instructions: Vec<&'static str> =
        (0u8..5).filter_map(|u| get_action(n & (1 << u))).collect();

    if n & 16u8 > 0 {
        instructions.iter().cloned().rev().collect()
    } else {
        instructions
    }
}
