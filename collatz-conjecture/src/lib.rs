pub fn collatz(n: u64) -> Option<u64> {
    if n < 1 {
        return None;
    }
    let mut n: u64 = n.clone();

    for i in 0.. {
        if n == 1 {
            return Some(i as u64);
        }
        let parity = n % 2;
        if parity == 0 {
            n /= 2;
        } else {
            let (m, did_overflow) = n.overflowing_mul(3);
            if did_overflow {
                break;
            }
            let (m, did_overflow) = m.overflowing_add(1);
            if did_overflow {
                break;
            }
            n = m;
        }
    }
    None
}
