pub struct PascalsTriangle {
    row_count: usize,
}

impl PascalsTriangle {
    pub fn new(row_count: u32) -> Self {
        Self {
            row_count: row_count as usize,
        }
    }

    pub fn rows(&self) -> Vec<Vec<u32>> {
        (1..=self.row_count).fold(Vec::new(), |mut v, i| {
            let mut row: Vec<u32> = vec![1];
            let last_row_inserted: Option<Vec<u32>> = v.last().cloned();
            match last_row_inserted {
                Some(last_row) => {
                    for window in last_row.windows(2) {
                        row.push(window[0] + window[1]);
                    }
                    if i > 1 {
                        row.push(1);
                    }
                }
                None => (),
            }

            v.push(row);
            v
        })
    }
}
