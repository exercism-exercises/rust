use std::collections::HashMap;

/// Count occurrences of words.
pub fn word_count(words: &str) -> HashMap<String, u32> {
    let mut result: HashMap<String, u32> = HashMap::new();
    for word in words
        .to_ascii_lowercase()
        .replace(|c: char| c != '\'' && !c.is_ascii_alphanumeric(), " ")
        .split_ascii_whitespace()
        .filter_map(|token| match token.trim_matches('\'') {
            "" => None,
            word => Some(word),
        })
    {
        *result.entry(word.to_string()).or_insert(0) += 1;
    }
    result
}
