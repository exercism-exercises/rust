///Define a Clock, only storing hours and minutes
#[derive(PartialEq, Debug)]
pub struct Clock {
    hours: i32,
    minutes: i32,
}

///Implement Clock trait so that our clock can handle both rollover and negative
impl Clock {

    ///Resolve minutes to a tuple of offset for hours and minutes of the hour, handling a negative entry
    fn resolve_minutes(minutes: i32) -> (i32, i32) {
        let offset_hours: i32 = minutes / 60;
        let resolved_minutes: i32 = minutes % 60;

        if resolved_minutes < 0 {(offset_hours - 1, 60 + resolved_minutes)} else {(offset_hours, resolved_minutes)}
    }

    ///Resolve hours within 24 hours, handling a negative entry
    fn resolve_hours(hours: i32, offset_hours: i32) -> i32 {
        let resolved_hours: i32 = (hours + offset_hours) % 24;

        if resolved_hours < 0 {24 + resolved_hours} else {resolved_hours}
    }

    pub fn new(hours: i32, minutes: i32) -> Self {
        let (offset_hours, resolved_minutes) = Self::resolve_minutes(minutes);
        let resolved_hours: i32 = Self::resolve_hours(hours, offset_hours);

        Self {
            hours: resolved_hours,
            minutes: resolved_minutes,
        }
    }

    pub fn add_minutes(&self, minutes: i32) -> Self {
        Self::new(self.hours, self.minutes+minutes)
    }


}

/// Implement ToSring trait for clock so that we can display the time
impl ToString for Clock {
    fn to_string(&self) -> String {
        format!("{:0>2}:{:0>2}", self.hours.to_string(), self.minutes.to_string())
    }
}

