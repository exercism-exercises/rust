#[derive(Debug, PartialEq, Eq)]
pub struct Dna {
    strand: Vec<char>,
}

#[derive(Debug, PartialEq, Eq)]
pub struct Rna {
    strand: Vec<char>,
}

impl Dna {
    pub fn new(dna: &str) -> Result<Dna, usize> {
        let mut strand: Vec<char> = Vec::new();
        for (u, nucleotide) in dna.chars().enumerate() {
            let nucleotide = match nucleotide {
                'G' | 'C' | 'T' | 'A' => Some(nucleotide),
                _ => None,
            };

            if nucleotide.is_none() {
                return Err(u);
            }

            strand.push(nucleotide.unwrap());
        }
        Ok(Dna { strand })
    }

    pub fn into_rna(self) -> Rna {
        Rna {
            strand: self
                .strand
                .iter()
                .map(|c| match c {
                    'G' => 'C',
                    'C' => 'G',
                    'T' => 'A',
                    'A' => 'U',
                    _ => panic!(),
                })
                .collect(),
        }
    }
}

impl Rna {
    pub fn new(rna: &str) -> Result<Rna, usize> {
        let mut strand: Vec<char> = Vec::new();
        for (u, nucleotide) in rna.chars().enumerate() {
            let nucleotide = match nucleotide {
                'G' | 'C' | 'U' | 'A' => Some(nucleotide),
                _ => None,
            };

            if nucleotide.is_none() {
                return Err(u);
            }

            strand.push(nucleotide.unwrap());
        }
        Ok(Rna { strand })
    }
}
