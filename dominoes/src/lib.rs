fn flip(domino: (u8, u8)) -> (u8, u8) {
    (domino.1, domino.0)
}

fn backtrack(input: &[(u8, u8)], used: usize, track: Vec<(u8, u8)>) -> Option<Vec<(u8, u8)>> {
    
    for (u, d) in input.iter().enumerate().filter(|(u, _)| ((1 << u) & used) == 0) {
        let mut track = track.clone();
        let &(_, d1_2) = track.last().unwrap();
        if d1_2 == d.0 {
            track.push(*d);
        } else if d1_2 == d.1 {
            track.push(flip(*d))
        } else {
            continue;
        }
        let used = used | (1 << u);
        // leaf condition
        if used.count_ones() as usize == input.len() {
            let first = track.first().unwrap().0;
            let last = track.last().unwrap().1;
            if first == last {
                return Some(track);
            }
            else {
                return None
            }
        }

        let result = backtrack(input, used, track);
        if result.is_some() {
            return result;
        }
    }
    None
}

/// develop a backtracking algorithm
/// This solution works as follows:
/// 1. We start by trying each domino as the first piece in the chain.
/// 2. For each subsequent piece, we try to match it with the last piece in the current chain.
/// 3. We consider both orientations of each domino (original and flipped).
/// 4. If we can't find a match, we backtrack and try a different combination.
/// 5. We keep track of which dominoes we've used with the `used` vector to avoid using the same domino twice.
/// 6. If we successfully create a chain with all dominoes and the first and last numbers match, we return the solution.

// This algorithm will find a valid chain if one exists, or return `None` if no valid chain can be formed.

// Note that this solution assumes that the `Domino` struct is defined as shown at the beginning. You might need to adjust the code slightly depending on how the `Domino` type is actually defined in your problem setup.
pub fn chain(input: &[(u8, u8)]) -> Option<Vec<(u8, u8)>> {
    let n = input.len();
    if input.is_empty() {
        return Some(Vec::new());
    } else if n == 1 {
        return if input[0].0 == input[0].1 {Some(input.to_vec())} else {None};
    } 

    for (u, domino) in input.iter().enumerate() {
        let used: usize = 1 << u;
        let result1 = backtrack(input, used, vec![*domino]);
        if result1.is_some() {
            return result1;
        }
        let result2 = backtrack(input, used, vec![flip(*domino)]);

        if result2.is_some() {
            return result2;
        }
    }
    None
}
