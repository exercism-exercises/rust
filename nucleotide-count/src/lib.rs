use std::collections::HashMap;

const A: char = 'A';
const T: char = 'T';
const G: char = 'G';
const C: char = 'C';
const NUCLEOTIDES: [char; 4] = [A, T, G, C];

pub fn count(nucleotide: char, dna: &str) -> Result<usize, char> {
    let mut count: usize = 0;
    for c in dna.chars() {
        if nucleotide != A && nucleotide != T && nucleotide != G && nucleotide != C {
            return Err(nucleotide);
        }
        else if c != A && c != T && c != G && c != C {
            return Err(c);
        } 
        else if c == nucleotide {
            count += 1;
        }
    }
    Ok(count)
}

pub fn nucleotide_counts(dna: &str) -> Result<HashMap<char, usize>, char> {
    let mut result: HashMap<char, usize> = HashMap::new();
    for &nucleotide in NUCLEOTIDES.iter() {
        match count(nucleotide, dna) {
            Err(x) => {
                return Err(x);
            }
            Ok(s) => {
                result.insert(nucleotide, s);
            }
        }
    }
    Ok(result)
}
