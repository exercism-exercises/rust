pub fn raindrops(n: u32) -> String {

    let multiple_of_3 = n % 3 == 0;
    let multiple_of_5 = n % 5 == 0;
    let multiple_of_7 = n % 7 == 0;

    if !multiple_of_3 && !multiple_of_5 && !multiple_of_7 {
        return format!("{n}");
    }

    let mut result: String = String::new();

    if multiple_of_3 {
        result.push_str("Pling");
    } 
    if multiple_of_5 {
        result.push_str("Plang");
    }
    if multiple_of_7 {
        result.push_str("Plong");
    }
    result
}
