use std::collections::HashSet;
/// Determine whether a sentence is a pangram.
pub fn is_pangram(sentence: &str) -> bool {
    let alphabets_used: HashSet<char> = sentence
        .chars()
        .filter(|c| c.is_alphabetic())
        .map(|c| c.to_ascii_lowercase())
        .collect::<HashSet<char>>();

    ('a'..='z').all(|c| alphabets_used.contains(&c))
}
