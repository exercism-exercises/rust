use regex::Regex;

static BASE: &str = "base";
static OPERATIONS: &str = "operations";
static POWERS: &str = "powers";

pub fn answer(command: &str) -> Option<i32> {
    let regex_pattern = Regex::new(
        r"What is (?<base>-?[1-9][0-9]*)(?<operations>( (plus (-?[1-9][0-9]*)|minus (-?[1-9][0-9]*)|multiplied by (-?[1-9][0-9]*)|divided by (-?[1-9][0-9]*)))*)(?<powers>( raised to the (([1-9]*0th|[1-9]*1st|[1-9]*2nd|[1-9]*3rd|[1-9]*[4-9]th) power))*)\?").unwrap();
    regex_pattern.captures(command).and_then(|captures| {
        let mut result: Option<i32> = captures
            .name(BASE)
            .and_then(|base| base.as_str().parse::<i32>().ok());
        let operations = captures
            .name(OPERATIONS)
            .map(|m| {
                String::from(m.as_str())
                    .replace("multiplied by", "multiplied_by")
                    .replace("divided by", "divided_by")
                    .trim()
                    .split_ascii_whitespace()
                    .map(String::from)
                    .collect::<Vec<String>>()
            })
            .unwrap_or_default();
        for chunk in operations.chunks(2) {
            let operation = chunk[0].as_str();
            let value: Option<i32> = chunk[1].parse().ok();
            result = match operation {
                "plus" => result.and_then(|r| value.map(|v| r + v)),
                "minus" => result.and_then(|r| value.map(|v| r - v)),
                "multiplied_by" => result.and_then(|r| value.map(|v| r * v)),
                "divided_by" => result.and_then(|r| value.map(|v| r / v)),
                _ => None,
            };
        }
        let powers = captures
            .name(POWERS)
            .map(|m| {
                String::from(m.as_str())
                    .replace("th", "")
                    .replace("rd", "")
                    .replace("nd", "")
                    .replace("st", "")
                    .trim()
                    .split_ascii_whitespace()
                    .map(String::from)
                    .collect::<Vec<String>>()
            })
            .unwrap_or_default();
        for power in powers
            .iter()
            .enumerate()
            .filter(|(u, t)| u % 5 == 3)
            .map(|(_, t)| t)
        {
            result = result.and_then(|r| power.parse::<u32>().ok().map(|v| r.pow(v)));
        }
        result
    })
}
