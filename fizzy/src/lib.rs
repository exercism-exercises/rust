use std::ops::Rem;

// the PhantomData instances in this file are just to stop compiler complaints
// about missing generics; feel free to remove them

/// A Matcher is a single rule of fizzbuzz: given a function on T, should
/// a word be substituted in? If yes, which word?
pub struct Matcher<T> {
    rule: Box<dyn Fn(T) -> bool>,
    substitute: String,
}

impl<T> Matcher<T> {
    pub fn new<F, S>(matcher: F, substitute: S) -> Matcher<T>
    where
        F: Fn(T) -> bool + 'static,
        S: ToString,
    {
        Self {
            rule: Box::new(matcher),
            substitute: substitute.to_string(),
        }
    }

    pub fn apply(&self, value: T) -> Option<String> {
        if (self.rule)(value) {
            Some(self.substitute.clone())
        } else {
            None
        }
    }
}

/// A Fizzy is a set of matchers, which may be applied to an iterator.
///
/// Strictly speaking, it's usually more idiomatic to use `iter.map()` than to
/// consume an iterator with an `apply` method. Given a Fizzy instance, it's
/// pretty straightforward to construct a closure which applies it to all
/// elements of the iterator. However, we're using the `apply` pattern
/// here because it's a simpler interface for students to implement.
///
/// Also, it's a good excuse to try out using impl trait.
#[derive(Default)]
pub struct Fizzy<T> {
    matchers: Vec<Matcher<T>>,
}

impl<T> Fizzy<T>
where
    T: Copy + ToString + 'static,
{
    pub fn new() -> Self {
        Self {
            matchers: Vec::new(),
        }
    }

    // feel free to change the signature to `mut self` if you like
    #[must_use]
    pub fn add_matcher(mut self, matcher: Matcher<T>) -> Self {
        self.matchers.push(matcher);
        self
    }

    /// map this fizzy onto every element of an iterator, returning a new iterator
    pub fn apply<I>(self, iter: I) -> impl Iterator<Item = String>
    where
        I: Iterator<Item = T>,
    {
        // todo!() doesn't actually work, here; () is not an Iterator
        // that said, this is probably not the actual implementation you desire
        iter.map(move |item| {
            let value: String = self
                .matchers
                .iter()
                .filter_map(|matcher| matcher.apply(item))
                .collect();
            if value.is_empty() {
                item.to_string()
            } else {
                value
            }
        })
    }
}

/// convenience function: return a Fizzy which applies the standard fizz-buzz rules
pub fn fizz_buzz<T>() -> Fizzy<T>
where
    T: Clone + Copy + Default + ToString + Rem<Output = T> + PartialEq + From<u8> + 'static,
{
    Fizzy::<T>::new()
        .add_matcher(Matcher::new(
            |n: T| n.rem(T::from(3)) == T::default(),
            "fizz",
        ))
        .add_matcher(Matcher::new(
            |n: T| n.rem(T::from(5)) == T::default(),
            "buzz",
        ))
}
