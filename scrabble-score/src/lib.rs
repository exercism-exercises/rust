use std::ascii::AsciiExt;

/// Compute the Scrabble score for a word.
/// 
/// | ---------------------------- | ----- |
/// | A, E, I, O, U, L, N, R, S, T | 1     |
/// | D, G                         | 2     |
/// | B, C, M, P                   | 3     |
/// | F, H, V, W, Y                | 4     |
/// | K                            | 5     |
/// | J, X                         | 8     |
/// | Q, Z                         | 10    |
pub fn score(word: &str) -> u64 {
    word.to_ascii_uppercase()
        .chars()
        .map(|c| match c {
            'A' | 'E' | 'I' | 'O' | 'U' | 'L' | 'N' | 'R' | 'S' | 'T' => 1u64,
            'D' | 'G' => 2u64,
            'B' | 'C' | 'M' | 'P' => 3u64,
            'F' | 'H' | 'V' | 'W' | 'Y' => 4u64,
            'K' => 5u64,
            'J' | 'X' => 8u64,
            'Q' | 'Z' => 10u64,
            _ => 0u64,
        })
        .sum()
}
