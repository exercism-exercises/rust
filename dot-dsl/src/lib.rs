macro_rules! attr_methods { { } => {
    pub fn with_attrs(mut self, attrs: &[(&str, &str)]) -> Self {
            self.attrs = attrs
            .iter()
            .map(|&(k, v)| (String::from(k), String::from(v)))
            .collect();
            self
        }
        pub fn attr(&self, k: &str) -> Option<&str> {
            self.attrs.get(k).map(|x| x.as_str())
        }
    }
}

pub mod graph {

    type Attrs = HashMap<String, String>;
    use crate::graph::graph_items::edge::Edge;
    use crate::graph::graph_items::node::Node;
    use std::collections::HashMap;

    pub mod graph_items {

        pub mod node {

            use crate::graph::Attrs;
            use crate::graph::HashMap;

            #[derive(PartialEq, Eq, Clone, Debug)]
            pub struct Node {
                pub name: String,
                pub attrs: Attrs,
            }
            impl Node {
                pub fn new(name: &str) -> Self {
                    Self {
                        name: String::from(name),
                        attrs: HashMap::new(),
                    }
                }

                attr_methods!();
            }
        }

        pub mod edge {

            use crate::graph::Attrs;
            use crate::graph::HashMap;

            #[derive(PartialEq, Eq, Clone, Debug)]
            pub struct Edge {
                pub start: String,
                pub end: String,
                pub attrs: Attrs,
            }
            impl Edge {
                pub fn new(start: &str, end: &str) -> Self {
                    Self {
                        start: String::from(start),
                        end: String::from(end),
                        attrs: HashMap::new(),
                    }
                }

                attr_methods!();
            }
        }
    }

    #[derive(Default)]
    pub struct Graph {
        pub nodes: Vec<Node>,
        pub edges: Vec<Edge>,
        pub attrs: Attrs,
    }

    impl Graph {
        pub fn new() -> Self {
            Self::default()
        }

        pub fn with_nodes(mut self, nodes: &Vec<Node>) -> Self {
            self.nodes = nodes.clone();
            self
        }

        pub fn with_edges(mut self, edges: &Vec<Edge>) -> Self {
            self.edges = edges.clone();
            self
        }

        pub fn node(&self, name: &str) -> Option<Node> {
            self.nodes
                .iter()
                .find(|node| node.name == String::from(name))
                .cloned()
        }

        attr_methods!();
    }
}
