const RADIX: u32 = 10;
/// Check a Luhn checksum.
pub fn is_valid(code: &str) -> bool {
    let code = code.chars().filter(|c| !c.is_whitespace()).collect::<Vec<char>>();
    let size: usize = code.len();

    if size < 2 {
        return false;
    }

    let mut result: u32 = 0;

    for (i, c) in code.iter().rev().enumerate() {
        if !c.is_digit(RADIX) {
            return false;
        }

        if c.is_digit(RADIX) {
            let d: u32 = c.to_digit(RADIX).unwrap();
            if i % 2 == 1 {
                result += 2 * d - if d > 4 {9} else {0};
            } else {
                result += d;
            }
        }
    }
    result % 10 == 0
}
