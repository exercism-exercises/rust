
const TEN: u64 = 10;
/// `Palindrome` is a newtype which only exists when the contained value is a palindrome number in base ten.
///
/// A struct with a single field which is used to constrain behavior like this is called a "newtype", and its use is
/// often referred to as the "newtype pattern". This is a fairly common pattern in Rust.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Palindrome(u64);

impl Palindrome {
    /// Create a `Palindrome` only if `value` is in fact a palindrome when represented in base ten. Otherwise, `None`.
    pub fn new(value: u64) -> Option<Palindrome> {
        // we avoid converting to &str or String here
        // and take advatage of the fact that it's an integer
        let mut cloned_value: u64 = value.clone();
        let mut reversed_value: u64 = 0u64;

        // we essentially check value == reverse_digit(value)
        // reversed_value := reverse_digit(value)
        while cloned_value > 0 {
            reversed_value *= TEN;
            reversed_value += cloned_value % TEN;
            cloned_value /= TEN
        }
        // value == reverse_digit(value)
        if reversed_value == value {Some(Palindrome(value))} else {None}
    }

    /// Get the value of this palindrome.
    pub fn into_inner(self) -> u64 {
        self.0
    }

}

pub fn palindrome_products(min: u64, max: u64) -> Option<(Palindrome, Palindrome)> {
    let mut min_palindrome: Option<Palindrome> = None;
    let mut max_palindrome: Option<Palindrome> = None;
    for i in min..(max+1) {
        for j in i..(max+1) {
            let p = Palindrome::new(i.saturating_mul(j));
            if p == None {
                continue;
            }
            min_palindrome = match (min_palindrome, p.clone()) {
                (_, None) => min_palindrome,
                (None, Some(v)) => Some(v),
                (Some(v1), Some(v2)) => if v1.into_inner() <= v2.into_inner() {Some(v1)} else {Some(v2)},
            };
            max_palindrome = match (max_palindrome, p.clone()) {
                (_, None) => max_palindrome,
                (None, Some(v)) => Some(v),
                (Some(v1), Some(v2)) => if v1.into_inner() <= v2.into_inner() {Some(v2)} else {Some(v1)},
            };
        }
    }
    match (min_palindrome, max_palindrome) {
        (Some(v1), Some(v2)) => Some((v1, v2)),
        _ => None
    }
}
