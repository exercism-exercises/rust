const TEN: u32 = 10;

pub struct Luhn {
    digits: Vec<Option<u32>>,
}

impl Luhn {
    pub fn is_valid(&self) -> bool {
        let size = self.digits.len();
        let filtered_digits = self.digits.iter().flatten().cloned().collect::<Vec<u32>>();
        if size != filtered_digits.len() {
            return false;
        } else if size < 2 {
            return false;
        }

        let mut result: u32 = 0;

        for (i, &d) in filtered_digits.iter().rev().enumerate() {
            if i % 2 == 1 {
                result += if d > 4 { 2 * d - 9 } else { 2 * d };
            } else {
                result += d;
            }
        }
        result % 10 == 0
    }
}

/// Here is the example of how the From trait could be implemented
/// for the &str type. Naturally, you can implement this trait
/// by hand for the every other type presented in the test suite,
/// but your solution will fail if a new type is presented.
/// Perhaps there exists a better solution for this problem?
impl<'a> From<&'a str> for Luhn {
    fn from(input: &'a str) -> Self {
        let digits = input
            .chars()
            .filter(|c| !c.is_whitespace())
            .map(|c| c.to_digit(TEN))
            .collect::<Vec<Option<u32>>>();
        Self { digits: digits }
    }
}

impl From<String> for Luhn {
    fn from(input: String) -> Self {
        let digits = input
            .chars()
            .filter(|c| !c.is_whitespace())
            .map(|c| c.to_digit(TEN))
            .collect::<Vec<Option<u32>>>();
        Self { digits: digits }
    }
}

macro_rules! impl_luhn_for_numbers {
    ( $($type:ty),* $(,)? ) => {
        $(
            impl From<$type> for Luhn  {
                fn from(input: $type) -> Self {
                    let mut number: $type = input;
                    let mut vec: Vec<Option<u32>> = Vec::new();
                    while number > 0 {
                        vec.push(Some((number % 10) as u32));
                        number /= 10;
                    }

                    Self {
                        digits: vec.iter().cloned().rev().collect::<Vec<Option<u32>>>()
                    }
                }

            }
        )*
    };
}

impl_luhn_for_numbers!(u8, u16, u32, u64, u128, usize);
