use itertools::Itertools;
use std::collections::{HashMap, HashSet};

const EQ: &str = "==";
const PLUS: &str = "+";

fn eval(word: &str, config: &HashMap<char, u8>) -> u64 {
    let n: usize = word.len();
    word.chars()
        .enumerate()
        .map(|(i, c)| {
            let letter_value: u64 = (*config.get(&c).unwrap()) as u64;
            u64::saturating_mul(letter_value, u64::saturating_pow(10, (n - 1 - i) as u32))
        })
        .sum::<u64>()
}

pub fn solve(input: &str) -> Option<HashMap<char, u8>> {
    // ensure input is of form \sum_{i=1}^n s_i == S

    let lhs_and_rhs: Vec<&str> = input.split(EQ).collect();
    assert!(lhs_and_rhs.len() == 2);

    let alphabets: Vec<char> = input
        .chars()
        .filter(|c| c.is_alphabetic())
        .unique()
        .collect();

    let lhs: HashMap<&str, usize> = lhs_and_rhs[0].split(PLUS).map(|s| s.trim()).counts();
    let rhs: &str = lhs_and_rhs[1].trim();
    assert!(lhs.len() > 0);
    assert!(rhs.len() > 0);

    let mut head_chars: HashSet<char> = HashSet::new();
    head_chars.insert(rhs.chars().next().unwrap());
    for &word in lhs.keys() {
        head_chars.insert(word.chars().next().unwrap());
    }

    let perms = (0u8..10).permutations(alphabets.len());

    for p in perms {
        let config: HashMap<char, u8> = alphabets
            .iter()
            .zip(p)
            .map(|(&k, v)| (k, v))
            .collect();
        if head_chars.iter().any(|  c| *config.get(c).unwrap_or(&11u8) == 0u8) {
            continue;
        }
        let rhs_value: u64 = eval(rhs, &config);
        let lhs_value: u64 = lhs.iter().map(|(&word, &count)| (count as u64) * eval(word, &config)).sum();
        if lhs_value == rhs_value {
            return Some(config);
        }
    }

    None
}
