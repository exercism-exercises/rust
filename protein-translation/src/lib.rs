use std::collections::HashMap;

use itertools::Itertools;

pub struct CodonsInfo<'a> {
    // We fake using 'a here, so the compiler does not complain that
    // "parameter `'a` is never used". Delete when no longer needed.
    map: HashMap<&'a str, &'a str>,
}

impl<'a> CodonsInfo<'a> {
    pub fn name_for(&self, codon: &str) -> Option<&'a str> {
        self.map.get(codon).copied()
    }

    pub fn of_rna(&self, rna: &str) -> Option<Vec<&'a str>> {
        let mut result: Vec<&'a str> = Vec::new();
        for chunk in rna.chars().chunks(3).into_iter() {
            let name: Option<&'a str> = self.name_for(chunk.collect::<String>().as_str());

            if name.is_none() {
                return None;
            } else {
                match name {
                    Some("stop codon") => {
                        break;
                    }
                    Some(x) => {
                        result.push(x);
                    }
                    None => panic!(),
                }
            }
        }

        Some(result)
    }
}

pub fn parse<'a>(pairs: Vec<(&'a str, &'a str)>) -> CodonsInfo<'a> {
    CodonsInfo {
        map: pairs.iter().map(|&(k, v)| (k, v)).collect(),
    }
}
