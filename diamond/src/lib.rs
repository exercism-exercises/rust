pub fn get_diamond(c: char) -> Vec<String> {
    let chars: Vec<char> = ('A'..=c).collect();
    let n = chars.len();
    let mut result: Vec<String> = chars
        .iter()
        .enumerate()
        .map(|(index, &letter)| {
            let mut line = String::new();
            for u in 0usize..(2 * n - 1) {
                let diff = if u <= (n - 1) { n - 1 - u } else { u - n + 1 };
                if diff == index {
                    line.push(letter);
                } else {
                    line.push(' ');
                }
            }
            line
        })
        .collect();

    let chars: Vec<char> = ('A'..c).collect();

    for (index, &letter) in chars.iter().enumerate().rev() {
        let mut line = String::new();
        for u in 0usize..(2 * n - 1) {
            let diff = if u <= (n - 1) { n - 1 - u } else { u - n + 1 };
            if diff == index {
                line.push(letter);
            } else {
                line.push(' ');
            }
        }
        result.push(line);
    }

    result
}
