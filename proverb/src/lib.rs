pub fn build_proverb(list: &[&str]) -> String {
    let mut result = String::new();
    let n = list.len();
    if n > 1 {
        for window in list.windows(2) {
            result.push_str(&format!("For want of a {} the {} was lost.\n", window[0], window[1]))
        }
    }

    if n > 0 {
        result.push_str(&format!("And all for the want of a {}.", list[0]))
    }

    result
}
