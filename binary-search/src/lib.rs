pub fn find(array: &[i32], key: i32) -> Option<usize> {
    let n = array.len();
    if n == 0 {
        return None;
    }

    let mut l: usize = 0;
    let mut r: usize = n - 1;

    while r - l > 1 {
        let c: usize = (l + r) / 2;
        if array[c] == key {
            return Some(c);
        }
        else if key < array[l] || key > array[r] || array[l] == key || array[r] == key || array[c] == key {
            break;
        } else if key < array[c] {
            r = c;
        } else if key > array[c] {
            l = c;
        }
    }
    if array[l] == key {
        return Some(l);
    } else if array[r] == key {
        return Some(r);
    }
    None
}
