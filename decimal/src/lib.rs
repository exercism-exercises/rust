use std::cmp::{Ordering, PartialEq, PartialOrd};
use std::ops::{Add, Mul, Sub};

/// Type implementing arbitrary-precision decimal arithmetic

#[derive(Debug, Clone, Eq)]
pub struct Decimal {
    digits: Vec<u8>,
    digit_one: usize,
    negative: bool,
}

impl Decimal {
    /// no numbers of the form .2374
    /// no preceding zero assumed
    pub fn try_from(input: &str) -> Option<Decimal> {
        let mut explicit_sign: bool = false;
        let mut negative: bool = false;
        let mut whole: bool = true;
        let mut digit_one: usize = 0;
        let mut digits: Vec<u8> = Vec::new();
        let mut invalid: bool = false;

        for (u, c) in input.char_indices() {
            match c {
                '-' => {
                    negative = true;
                    explicit_sign = true;
                }
                '.' => {
                    digit_one = u.saturating_sub(if explicit_sign {2} else {1});
                    whole = false;
                }
                '0'..='9' => digits.push(c as u8 - b'0'),
                '+' => {
                    explicit_sign = true;
                },
                _ => {
                    invalid = false;
                    break;
                }
            }
        }
        if invalid {
            return None;
        }
        if digits.is_empty() {
            digits.push(0);
        }
        if whole {
            digit_one = digits.len()-1;
        }
        
        Some(
            Decimal {
                negative,
                digits,
                digit_one,
            }
            .trim(),
        )
    }

    fn trim(&self) -> Self {
        let mut u: usize = 0;
        let mut v: usize = self.digits.len() - 1;
        let mut w: usize = self.digit_one;

        while self.digits[u] == 0 && u < self.digit_one {
            u += 1;
            w = w.saturating_sub(1);
        }

        while self.digits[v] == 0 && self.digit_one < v {
            v = v.saturating_sub(1);
        }

        Self {
            negative: !self.is_zero() && self.negative,
            digit_one: w,
            digits: self.digits[u..v + 1].to_vec(),
        }
    }

    fn is_zero(&self) -> bool {
        self.digits.iter().all(|&d| d == 0)
    }

    fn is_one(&self) -> bool {
        let this = self.trim();
        !this.negative && this.digits.len() == 1 && this.digits[0] == 1
    }

    fn negate(self) -> Self {
        Self {
            negative: !self.negative,
            digits: self.digits,
            digit_one: self.digit_one,
        }
    }

    fn abs(&self) -> Self {
        Self {
            negative: false,
            digits: self.digits.clone(),
            digit_one: self.digit_one,
        }
    }

    fn naive_add(&self, other: Self) -> Self {
        assert_eq!(self.negative, other.negative);
        let mut fraction_digits_rev: Vec<u8> = Vec::new();
        let mut integer_digits_rev: Vec<u8> = Vec::new();
        let o1 = self.digit_one + 1;
        let o2 = other.digit_one + 1;
        //1220.004 -> 1220004, 3
        //7.0089 -> 70089, 0
        let mut carry: u8 = 0;
        //7 - 4 = 3
        //5 - 1 = 4
        for f in (0..=(self.digits.len() - o1).max(other.digits.len() - o2)).rev() {
            let f1: u8 = self.digits[o1..].get(f).cloned().unwrap_or_default();
            let f2: u8 = other.digits[o2..].get(f).cloned().unwrap_or_default();
            let sum = f1 + f2 + carry;
            carry = sum / 10;
            fraction_digits_rev.push(sum % 10);
        }

        for i in (0..o1.max(o2)).rev() {
            let i1: u8 = self.digits[..o1].get(i).cloned().unwrap_or_default();
            let i2: u8 = other.digits[..o2].get(i).cloned().unwrap_or_default();
            let sum = i1 + i2 + carry;
            carry = sum / 10;
            integer_digits_rev.push(sum % 10);
        }

        if carry > 0 {
            integer_digits_rev.push(carry);
        }

        let mut digits: Vec<u8> = Vec::new();

        let digit_one = integer_digits_rev.len() - 1;

        for i in integer_digits_rev.iter().rev() {
            digits.push(*i);
        }

        for f in fraction_digits_rev.iter().rev() {
            digits.push(*f);
        }

        Decimal {
            digits,
            negative: self.negative,
            digit_one,
        }
        .trim()
    }

    fn naive_sub(&self, other: Self) -> Self {
        assert_eq!(self.negative, other.negative);

        let self_abs = self.abs();
        let other_abs = other.abs();
        let mut fraction_digits_rev: Vec<u8> = Vec::new();
        let mut integer_digits_rev: Vec<u8> = Vec::new();
        let o1 = self.digit_one + 1;
        let o2 = other.digit_one + 1;
        //1000.2937 -> 10002937, 3
        //900.3 -> 9003, 2
        let mut carry: u8 = 0;
        //
        for f in (0..=(self.digits.len() - o1).max(other.digits.len() - o2)).rev() {
            let f1: u8 = self.digits[o1..].get(f).cloned().unwrap_or_default();
            let f2: u8 = other.digits[o2..].get(f).cloned().unwrap_or_default();
            let diff = if self_abs >= other_abs {
                10 + f1 - f2
            } else {
                10 + f2 - f1
            } - carry;
            carry = if diff < 10 { 1 } else { 0 };
            fraction_digits_rev.push(diff % 10);
        }

        for i in (0..o1.max(o2)).rev() {
            let i1: u8 = self.digits[..o1].get(i).cloned().unwrap_or_default();
            let i2: u8 = other.digits[..o2].get(i).cloned().unwrap_or_default();

            let diff = if self_abs >= other_abs {
                10 + i1 - i2
            } else {
                10 + i2 - i1
            } - carry;
            carry = if diff < 10 { 1 } else { 0 };
            integer_digits_rev.push(diff % 10);
        }

        let mut digits: Vec<u8> = Vec::new();

        let digit_one = integer_digits_rev.len() - 1;

        for i in integer_digits_rev.iter().rev() {
            digits.push(*i);
        }

        for f in fraction_digits_rev.iter().rev() {
            digits.push(*f);
        }

        Decimal {
            digits,
            negative: if self_abs < other_abs {
                !self.negative
            } else {
                self.negative
            },
            digit_one,
        }
        .trim()
    }

    fn scale(&self, coeff: u8) -> Self {
        let this = self.trim();
        if coeff == 0 {
            return Decimal {
                negative: false,
                digits: vec![0],
                digit_one: 0,
            };
        }
        let mut carry: u8 = 0;
        let mut digits_rev: Vec<u8> = Vec::new();
        let mut digit_one: usize = this.digit_one;

        for d in this.digits.iter().rev() {
            let v = d * coeff;
            digits_rev.push((v + carry) % 10);
            carry = (v + carry) / 10;

        }
        if carry > 0 {
            digits_rev.push(carry);
            digit_one += 1;
        }
        Self {
            negative: this.negative,
            digits: digits_rev.iter().rev().cloned().collect(),
            digit_one,
        }.trim()
    }

    fn left_shift(&self, by: usize) -> Self {
        let this = self.trim();
        let fractional_n = this.digits[this.digit_one+1..].len();
        if by == 0 {
            this
        }
        // 0.278, 4 -> 2780 // where we need to pad 0 on the right
        else if by > 0 && by > fractional_n {
            let mut digits = this.digits.clone();
            for _ in 0..(by - fractional_n) {
                digits.push(0);
            }
            Self {
                negative: this.negative,
                digits,
                digit_one: this.digit_one + by,
            }
        } 
        // 27.385, 2 -> 2738.5 // no padding needed
        else {       
            Self {
                negative: this.negative,
                digits: this.digits,
                digit_one: this.digit_one + by,
            }
        }
        

    }

    fn right_shift(&self, by: usize) -> Self {
        let this = self.trim();
        let fractional_n = this.digits[this.digit_one+1..].len();
        if by == 0 {
            this
        }
        // 2, 3 -> 0.002 // where we need to pad 0 on the left
        // 200, 4
        else if by > 0 && by > this.digit_one {
            let mut digits_rev: Vec<u8> = this.digits.iter().cloned().rev().collect();
            for _ in 0..(by - fractional_n) {
                digits_rev.push(0);
            }
            Self {
                negative: this.negative,
                digits: digits_rev.iter().rev().cloned().collect(),
                digit_one: this.digit_one,
            }        // 0.278, 4 -> 2780 // where we need to pad 0 on the right

        } 
        // 27.385, 2 -> 2738.5 // no padding needed
        else {       
            Self {
                negative: this.negative,
                digits: this.digits,
                digit_one: this.digit_one - by,
            }
        }
    }
}

impl PartialEq for Decimal {
    fn eq(&self, other: &Self) -> bool {
        let this = self.trim();
        let that = other.trim();
        this.negative == that.negative
            && this.digits == that.digits
            && this.digit_one == that.digit_one
    }
}

impl PartialOrd for Decimal {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        let this = self.trim();
        let that = other.trim();

        if this == that {
            return Some(Ordering::Equal);
        } else if this.negative && !that.negative {
            return Some(Ordering::Less);
        } else if !this.negative && that.negative {
            return Some(Ordering::Greater);
        }

        let magnitude_partial_cmp =
            (this.digit_one + 1)
                .partial_cmp(&(that.digit_one + 1))
                .map(|magnitude_cmp| match magnitude_cmp {
                    Ordering::Equal => match this.digits[0..this.digit_one + 1]
                        .cmp(&that.digits[0..that.digit_one + 1])
                    {
                        Ordering::Equal => this.digits[this.digit_one + 1..]
                            .cmp(&that.digits[that.digit_one + 1..]),
                        x => x,
                    },
                    x => x,
                });

        magnitude_partial_cmp.map(|magintude_cmp| match magintude_cmp {
            Ordering::Equal => Ordering::Equal,
            Ordering::Less => {
                if this.negative && that.negative {
                    Ordering::Greater
                } else {
                    Ordering::Less
                }
            }
            Ordering::Greater => {
                if this.negative && that.negative {
                    Ordering::Less
                } else {
                    Ordering::Greater
                }
            }
        })
    }
}

impl Add for Decimal {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        let this = self.trim();
        let that = other.trim();
        if that.is_zero() {
            this
        } else if this.is_zero() {
            that
        } else if this.negative == that.negative {
            this.naive_add(that)
        } else {
            (this - that.negate()).trim()
        }
    }
}

impl Sub for Decimal {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        let this = self.trim();
        let that = other.trim();
        if that.is_zero() {
            this
        } else if this.is_zero() {
            that.negate()
        } else if this == that {
            Decimal {
                negative: false,
                digits: vec![0],
                digit_one: 0,
            }
        } else if this.negative == that.negative {
            this.naive_sub(that)
        } else {
            (this + that.negate()).trim()
        }
    }
}

impl Mul for Decimal {
    type Output = Self;

    fn mul(self, other: Self) -> Self {
        let this = self.trim();
        let that = other.trim();
        if this.is_zero() || that.is_zero() {
            return Decimal {
                negative: false,
                digits: vec![0],
                digit_one: 0,
            };
        }
        if that.abs().is_one() {
            if that.negative {this.negate()} else {this}
        } else if this.abs().is_one() {
            if this.negative {that.negate()} else {that}
        } else {
            let mut result = Decimal {
                negative: false,
                digits: vec![0],
                digit_one: 0,
            };
            for (u, d) in that.digits.iter().enumerate() {
                if u <= that.digit_one {
                    let scaled = this.abs().scale(*d).left_shift(that.digit_one-u);
                    result = result + scaled;
                } else {
                    let scaled = this.abs().scale(*d).right_shift(u-that.digit_one);
                    result = result + scaled;
                }
            }
            Self {
                negative: this.negative != that.negative,
                digit_one: result.digit_one,
                digits: result.digits,
            }
        }
    }
}
