const CHECK: char = 'X';
const DASH: char = '-';

/// Determines whether the supplied string is a valid ISBN number
pub fn is_valid_isbn(isbn: &str) -> bool {
    let mut count: u32 = 0u32;
    let mut sum: u32 = 0u32;
    for c in isbn.chars() {
        let (is_valid, value) = match c {
            CHECK => (true, 10u32),
            DASH => (true, 11u32),
            c if c.is_digit(10) => (true, c.to_digit(10).unwrap()),
            _ => (false, 0u32),
        };
        if !is_valid || count > 10u32 || (count < 9u32 && value == 10) {
            return false;
        }
        if is_valid && value < 11u32 {
            sum += (10 - count) * value;
            count += 1;
        }
    }
    count == 10u32 && sum % 11 == 0
}
