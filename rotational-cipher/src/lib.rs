pub fn rotate(input: &str, key: u8) -> String {
    input
        .chars()
        .map(|c| match c {
            'a'..='z' => ((c as u8 - 'a' as u8 + 26 + key) % 26 + 'a' as u8) as char,
            'A'..='Z' => ((c as u8 - 'A' as u8 + 26 + key) % 26 + 'A' as u8) as char,
            _ => c,
        })
        .collect()
}
