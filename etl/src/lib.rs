use std::collections::BTreeMap;

pub fn transform(h: &BTreeMap<i32, Vec<char>>) -> BTreeMap<char, i32> {
    /**
     *     h.iter()
        .flat_map(|(&point, values)| values.iter().map(move |value| (value.to_ascii_lowercase(), point)))
        .collect()
     */
    let mut result: BTreeMap<char, i32> = BTreeMap::new();
    for (&point, values) in h.iter() {
        for value in values {
            let lower_case: char = value.to_lowercase().last().unwrap();
            result.insert(lower_case, point);
        }
    }
    result
}
