pub fn egg_count(display_value: u32) -> usize {
    // display_value.count_ones()
    (0u32..32)
        .map(|i| display_value & (1u32 << i))
        .filter(|&i| i > 0)
        .count()
}
