use ndarray::Array;

#[derive(Debug)]
pub struct Item {
    pub weight: u32,
    pub value: u32,
}

/// implements the classic 0-1 knapsack problem
/// with pseudo linear dynamic programming solution
/// m[[u, w]] w/ item u {v_u, w_u} = if w_u > w {m[[u-1,w]]} else {max(m[[u-1, w]], m[[u-1, w-w_u]] + v_u)}
pub fn maximum_value(max_weight: u32, items: &[Item]) -> u32 {
    let n = items.len();
    let m_w = max_weight as usize;
    let mut m = Array::<u32, _>::zeros((n + 1, m_w + 1));
    for u in 1..=n {
        for w in 0..=m_w {
            let v_u = items[u - 1].value;
            let w_u = items[u - 1].weight;
            if w_u as usize > w {
                m[[u, w]] = m[[u - 1, w]];
            } else {
                m[[u, w]] = m[[u - 1, w]].max(m[[u - 1, w - (w_u as usize)]] + v_u);
            }
        }
    }
    m[[n, m_w]]
}
