pub fn lowest_price(books: &[u32]) -> u32 {
    let mut counts: [u32; 5] = [0; 5];
    books.iter().for_each(|&x| counts[(x - 1) as usize] += 1);
    counts.sort();

    let fives = counts[0];
    let fours = counts[1] - counts[0];
    let threes = counts[2] - counts[1] ;
    let twos = counts[3] - counts[2];
    let ones = counts[4] - counts[3];
    let pairs_of_3_and_5 = threes.min(fives);
    800*ones + 1520*twos + 2160*(threes - pairs_of_3_and_5) + 2560*(fours + pairs_of_3_and_5*2) + 3000*(fives - pairs_of_3_and_5)
}
