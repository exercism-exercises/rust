#[derive(Debug, PartialEq, Eq)]
pub enum Error {
    InvalidInputBase,
    InvalidOutputBase,
    InvalidDigit(u32),
}

///
/// Convert a number between two bases.
///
/// A number is any slice of digits.
/// A digit is any unsigned integer (e.g. u8, u16, u32, u64, or usize).
/// Bases are specified as unsigned integers.
///
/// Return the corresponding Error enum if the conversion is impossible.
///
///
/// You are allowed to change the function signature as long as all test still pass.
///
///
/// Example:
/// Input
///   number: &[4, 2]
///   from_base: 10
///   to_base: 2
/// Result
///   Ok(vec![1, 0, 1, 0, 1, 0])
///
/// The example corresponds to converting the number 42 from decimal
/// which is equivalent to 101010 in binary.
///
///
/// Notes:
///  * The empty slice ( "[]" ) is equal to the number 0.
///  * Never output leading 0 digits, unless the input number is 0, in which the output must be `[0]`.
///    However, your function must be able to process input with leading 0 digits.
///
pub fn convert(number: &[u32], from_base: u32, to_base: u32) -> Result<Vec<u32>, Error> {
    // errorneous case handling
    if from_base < 2 {
        return Err(Error::InvalidInputBase);
    } else if to_base < 2 {
        return Err(Error::InvalidOutputBase);
    }

    // handle empty lst
    if number.is_empty() {
        return Ok(vec![0]);
    }

    // sum the digits into s
    let n = number.len() - 1;
    let mut s: u32 = 0;

    for (i, &d) in number.iter().enumerate() {
        // if invalid digit, return error early
        if d >= from_base {
            return Err(Error::InvalidDigit(d));
        }
        // use saturating_pow to avoid overflow
        s += u32::saturating_pow(from_base, (n - i).try_into().unwrap()) * d
    }

    // our output
    let mut v: Vec<u32> = Vec::new();

    // accumulate to_base representation to our output v
    let l = (s as f32).log(to_base as f32).floor() as u32;
    for o in 0..(l + 1) {
        let b = u32::saturating_pow(to_base, (l - o).try_into().unwrap());
        let d = s / b;
        v.push(d);
        s = s % b;
    }

    // wrap our output to Ok(..)
    Ok(v)
}
