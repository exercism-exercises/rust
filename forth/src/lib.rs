pub type Value = i32;
pub type Result = std::result::Result<(), Error>;

pub struct Forth {
    stack: Vec<Value>,
    words: Vec<(String, String)>,
}

#[derive(Debug, PartialEq, Eq)]
pub enum Error {
    DivisionByZero,
    StackUnderflow,
    UnknownWord,
    InvalidWord,
}

impl Forth {
    pub fn new() -> Forth {
        Forth {
            stack: vec![],
            words: vec![],
        }
    }

    pub fn stack(&self) -> &[Value] {
        &self.stack
    }

    pub fn eval(&mut self, input: &str) -> Result {
        if input.starts_with(':') && input.ends_with(';') {
            let input = input[1..input.len() - 1].trim();
            let Some(split_index) = input.find(' ') else {
                return Err(Error::InvalidWord);
            };
            let word = input[..split_index].trim().to_ascii_lowercase();
            if word.parse::<Value>().is_ok() {
                return Err(Error::InvalidWord);
            }
            self.words
                .push((word, input[split_index + 1..].trim().to_ascii_lowercase()));
            return Ok(());
        }
        let input = self.replace_input(input.to_ascii_lowercase());
        for c in input.split_whitespace() {
            match c {
                "+" => {
                    let v1 = self.stack.pop().ok_or(Error::StackUnderflow)?;
                    let v2 = self.stack.pop().ok_or(Error::StackUnderflow)?;
                    self.stack.push(v1 + v2);
                }
                "-" => {
                    let v1 = self.stack.pop().ok_or(Error::StackUnderflow)?;
                    let v2 = self.stack.pop().ok_or(Error::StackUnderflow)?;
                    self.stack.push(v2 - v1);
                }
                "*" => {
                    let v1 = self.stack.pop().ok_or(Error::StackUnderflow)?;
                    let v2 = self.stack.pop().ok_or(Error::StackUnderflow)?;
                    self.stack.push(v1 * v2);
                }
                "/" => {
                    let v1 = self.stack.pop().ok_or(Error::StackUnderflow)?;
                    if v1 == 0 {
                        return Err(Error::DivisionByZero);
                    }
                    let v2 = self.stack.pop().ok_or(Error::StackUnderflow)?;
                    self.stack.push(v2 / v1);
                }
                "dup" => {
                    self.stack
                        .push(*self.stack.last().ok_or(Error::StackUnderflow)?);
                }
                "drop" => {
                    self.stack.pop().ok_or(Error::StackUnderflow)?;
                }
                "swap" => {
                    let v1 = self.stack.pop().ok_or(Error::StackUnderflow)?;
                    let v2 = self.stack.pop().ok_or(Error::StackUnderflow)?;
                    self.stack.push(v1);
                    self.stack.push(v2);
                }
                "over" => {
                    if self.stack.len() < 2 {
                        return Err(Error::StackUnderflow);
                    }
                    let second_top_index = self.stack.len() - 2;
                    self.stack
                        .push(*self.stack.iter().nth(second_top_index).unwrap());
                }
                _ if c.parse::<Value>().is_ok() => self.stack.push(c.parse::<Value>().unwrap()),
                _ => return Err(Error::UnknownWord),
            }
        }
        return Ok(());
    }

    fn replace_input(&self, input: String) -> String {
        self.words.iter().rev().fold(input, |mut acc, x| {
            acc = acc.replace(&x.0, &x.1);
            return acc;
        })
    }
}
