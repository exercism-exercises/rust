#[derive(Debug, PartialEq, Eq)]
pub enum Error {
    SpanTooLong,
    InvalidDigit(char),
}

pub fn lsp(string_digits: &str, span: usize) -> Result<u64, Error> {

    if span > string_digits.len() {
        return Err(Error::SpanTooLong);
    }
    let mut largest: u64 = 0;
    let digits: Vec<char> = string_digits.chars().collect();
    for window in digits.windows(span) {
        let mut product: u64 = 1;
        for &c in window {
            let digit = c.to_digit(10);
            if digit.is_none() {
                return Err(Error::InvalidDigit(c));
            }
            product *= digit.unwrap() as u64;
        }
        if product > largest {
            largest = product;
        }
    }
    Ok(largest)
}
