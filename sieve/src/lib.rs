pub fn primes_up_to(upper_bound: u64) -> Vec<u64> {
    let mut sieve: Vec<bool> = (0..=upper_bound).map(|_| false).collect();
    sieve[0] = true;
    sieve[1] = true;

    let mut u: usize = 2;

    while u * u <= upper_bound as usize {
        for v in (2 * u..=upper_bound as usize).step_by(u) {
            sieve[v] = true;
        }
        u += 1;
    }

    sieve
        .iter()
        .enumerate()
        .filter(|(_, &b)| !b)
        .map(|(u, _)| u as u64)
        .collect()
}
