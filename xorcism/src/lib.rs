use std::borrow::Borrow;

/// A munger which XORs a key with some data
#[derive(Clone)]
pub struct Xorcism<'a> {
    /// the key
    key: &'a [u8],
    offset: usize,
}

impl<'a> Xorcism<'a> {
    /// Create a new Xorcism munger from a key
    ///
    /// Should accept anything which has a cheap conversion to a byte slice.
    /// Self type not allowed
    pub fn new<Key>(key: &'a Key) -> Xorcism<'a>
    where
        Key: AsRef<[u8]> + ?Sized,
    {
        Xorcism {
            key: key.as_ref(),
            offset: 0,
        }
    }

    /// XOR each byte of the input buffer with a byte from the key.
    ///
    /// Note that this is stateful: repeated calls are likely to produce different results,
    /// even with identical inputs.
    pub fn munge_in_place(&mut self, data: &mut [u8]) {
        data.iter_mut()
            .zip(self.key.iter().cycle().skip(self.offset))
            .for_each(|(data_entry, key_entry)| *data_entry ^= *key_entry);
        self.offset += data.len();
        self.offset %= self.key.len();
    }

    /// XOR each byte of the data with a byte from the key.
    ///
    /// Note that this is stateful: repeated calls are likely to produce different results,
    /// even with identical inputs.
    ///
    /// Should accept anything which has a cheap conversion to a byte iterator.
    /// Shouldn't matter whether the byte iterator's values are owned or borrowed.
    pub fn munge<'b, Data: IntoIterator>(&'b mut self, data: Data) -> impl Iterator<Item = u8> + '_
    where
        Data::IntoIter: 'b,
        Data::Item: Borrow<u8>,
    {
        //Do not call .into_iter() on references. More info on clippy.
        //Also, it is not pretty to put a Cycle<Iter<>> into a field, and that kinda defeats the whole exercise... 
        // The objective was to use generics for that, not declaring the concrete type. Anyway, here is mine if someone is interested (no "Captures" trait necessary).

        let munged = data
            .into_iter()
            .zip(self.key.iter().copied().cycle().skip(self.offset))
            .map(|(data_entry, key_entry)| {
                self.offset += 1;
                self.offset %= self.key.len();
                data_entry.borrow() ^ key_entry
            });

        munged
    }
}
