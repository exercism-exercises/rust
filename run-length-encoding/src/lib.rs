fn append(result: &mut String, counter: usize, char_to_compare: char) {
    if counter > 0 {
        if counter > 1 {
            result.push_str(counter.to_string().as_str());
        }
        result.push(char_to_compare)
    }
}

pub fn encode(source: &str) -> String {
    let mut result: String = String::new();
    let mut counter: usize = 0;
    let mut char_to_compare: char = '_';

    for c in source.chars() {
        if c != char_to_compare {
            // every time a different char is met, append the previous counter and previous char
            append(&mut result, counter, char_to_compare);
            // reset the counter to 1 and update the char to compare
            counter = 1;
            char_to_compare = c;
        } else {
            counter += 1;
        }
    }
    // append the last counter and last char
    append(&mut result, counter, char_to_compare);
    result
}

const DECIMAL: u32 = 10;

pub fn decode(source: &str) -> String {
    let mut result: String = String::new();
    let mut counter: u32 = 0;
    for c in source.chars() {
        match c.to_digit(DECIMAL) {
            // e.g.) 12 in 12W
            Some(x) => {
                counter *= DECIMAL;
                counter += x;
            },
            // e.g.) W at the end of 12W
            None if counter > 0 => {
                for _ in 0..counter {
                    result.push(c);
                }
                counter = 0;
            },
            // e.g.) X in XYZ
            None if counter == 0 => {
                result.push(c);
            },
            _ => panic!()
        }
    }
    result
}
