

/// What should the type of _function be?
pub fn map<I, O>(input: Vec<I>, mut function: impl FnMut(I) -> O) -> Vec<O> {
    let mut output: Vec<O> = Vec::new();

    for i in input {
        output.push(function(i));
    }

    output
}
