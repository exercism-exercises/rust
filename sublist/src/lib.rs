//use ndarray::Array2;
//use std::cmp;

#[derive(Debug, PartialEq, Eq)]
pub enum Comparison {
    Equal,
    Sublist,
    Superlist,
    Unequal,
}

pub fn sublist<T: PartialEq>(_first_list: &[T], _second_list: &[T]) -> Comparison {
    let c1 = _first_list.len();
    let c2 = _second_list.len();

    if _first_list == _second_list {
        Comparison::Equal
    } else if c1 < c2 && _first_list.is_empty()
        || _second_list.windows(c1).any(|s2| _first_list == s2)
    {
        Comparison::Sublist
    } else if c2 < c1 && _second_list.is_empty()
        || _first_list.windows(c2).any(|s1| _second_list == s1)
    {
        Comparison::Superlist
    } else {
        Comparison::Unequal
    }
}

/*
pub fn sublist<T: PartialEq>(_first_list: &[T], _second_list: &[T]) -> Comparison {
    let first_size = _first_list.len();
    let second_size = _second_list.len();

    if _first_list == _second_list {
        return Comparison::Equal;
    } else if first_size == 0 && second_size > 0 {
        return Comparison::Sublist;
    } else if first_size > 0 && second_size == 0 {
        return Comparison::Superlist;
    }

    return proper_sublist(_first_list, _second_list);
}


fn proper_sublist<T: PartialEq>(_first_list: &[T], _second_list: &[T]) -> Comparison {
    // dp_array[[i, j]]: =
    // max length of subsequence that exists in both _first_list[0..i] _second_list[0..j]
    let first_size = _first_list.len();
    let second_size = _second_list.len();
    let mut dp_array = Array2::<u32>::zeros((first_size, second_size));

    // Base cases

    // case 1: dp_array[[0, 0]]
    if _first_list[0] == _second_list[0] {
        dp_array[[0, 0]] = 1;
    }

    // case 2: dp_array[[i, 0]]
    for i in 1..first_size {
        dp_array[[i, 0]] =
            if _first_list[i] == _second_list[0] {
                1
            } else {
                0
            };
    }

    // case 3: dp_array[[0, j]]
    for j in 1..second_size {
        dp_array[[0, j]] =
            if _first_list[0] == _second_list[j] {
                1
            } else {
                0
            };
    }

    /* Inductive case
        dp_array[[i, j]] := f(
            dp_array[[i-1, j]],
            dp_array[[i, j-1]],
            dp_array[[i-1, j-1]]
        )
        rate crates
        [      c,r,a,t,e,s
            r [0,1,0,0,0,0],
            a [0,1,2,2,2,2],
            t [0,1,2,3,3,3],
            e [0,1,2,3,4,4]
        ]

        [      1,3
            1 [1,0],
            2 [0,1],
            3 [0,1]
        ]
    */
    for i in 1..first_size {
        for j in 1..second_size {
            let diagonal_update = dp_array[[i - 1, j - 1]]
                + if _first_list[i] == _second_list[j] {
                    1
                } else {
                    0
                };
            let above_update = dp_array[[i - 1, j]];
            let left_update = dp_array[[i, j - 1]];
            dp_array[[i, j]] =
                cmp::max::<u32>(diagonal_update, cmp::max::<u32>(left_update, above_update));
        }
    }

    let result = dp_array[[first_size - 1, second_size - 1]];

    if result == first_size as u32 {
        return Comparison::Sublist;
    } else if result == second_size as u32 {
        return Comparison::Superlist;
    }
    return Comparison::Unequal;
}
*/
