pub fn verse(n: u32) -> String {
    if n == 0 {
        return "No more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall.\n".to_string();
    } else if n == 1 {
        return "1 bottle of beer on the wall, 1 bottle of beer.\nTake it down and pass it around, no more bottles of beer on the wall.\n".to_string();
    }
    return format!("{} bottles of beer on the wall, {} bottles of beer.\nTake one down and pass it around, {} {} of beer on the wall.\n", n, n, n-1, if n > 2 {"bottles".to_string()} else {"bottle".to_string()}).to_string();
}

pub fn sing(start: u32, end: u32) -> String {
    if start < end {
        return "".to_string();
    } else {
        return (end..=start).rev().map(|n: u32| verse(n)).collect::<Vec<String>>().join("\n");
    }
}
