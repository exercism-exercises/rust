const THOUSAND: u64 = 1000;
const HUNDRED: u64 = 100;

fn encode_denominations(denomination: u64) -> String {
    match denomination {
        1_000_000_000_000_000_000 => "quintillion".to_owned(),
        1_000_000_000_000_000 => "quadrillion".to_owned(),
        1_000_000_000_000 => "trillion".to_owned(),
        1_000_000_000 => "billion".to_owned(),
        1_000_000 => "million".to_owned(),
        1_000 => "thousand".to_owned(),
        _ => "".to_owned()
    }
}

fn encode_ones(ones: u64) -> String {
    match ones {
        1 => "one".to_owned(),
        2 => "two".to_owned(),
        3 => "three".to_owned(),
        4 => "four".to_owned(),
        5 => "five".to_owned(),
        6 => "six".to_owned(),
        7 => "seven".to_owned(),
        8 => "eight".to_owned(),
        9 => "nine".to_owned(),
        _ => "".to_owned(),
    }
}

fn encode_tens_and_ones(tens_and_ones: u64) -> String {
    match tens_and_ones {
        10 => "ten".to_owned(),
        11 => "eleven".to_owned(),
        12 => "twelve".to_owned(),
        13 => "thirteen".to_owned(),
        14 => "fourteen".to_owned(),
        15 => "fifteen".to_owned(),
        16 => "sixteen".to_owned(),
        17 => "seventeen".to_owned(),
        18 => "eighteen".to_owned(),
        19 => "nineteen".to_owned(),
        tens_and_ones if tens_and_ones < 10 || (tens_and_ones >= 20 && tens_and_ones <= 99) => {
            let tens = tens_and_ones / 10;
            let ones = tens_and_ones % 10;
            let tens_said = match tens {
                2 => "twenty".to_owned(),
                3 => "thirty".to_owned(),
                4 => "forty".to_owned(),
                5 => "fifty".to_owned(),
                6 => "sixty".to_owned(),
                7 => "seventy".to_owned(),
                8 => "eighty".to_owned(),
                9 => "ninety".to_owned(),
                _ => "".to_owned()
            };
            let ones_said = encode_ones(ones);
            if tens > 0 && ones > 0 {format!("{}-{}", tens_said, ones_said)} else if tens > 0 {tens_said.to_owned()} else {ones_said.to_owned()}
        },
        _ => "".to_owned()
    }
}

fn encode_coefficient(coefficient:u64) -> String {
    let hundreds = (coefficient % THOUSAND) / HUNDRED;
    let tens_and_ones =  coefficient % HUNDRED;

    if hundreds > 0 && tens_and_ones > 0 {format!("{} hundred {}", encode_ones(hundreds), encode_tens_and_ones(tens_and_ones))} else if hundreds > 0 {format!("{} hundred", encode_ones(hundreds))} else {encode_tens_and_ones(tens_and_ones)}
}

pub fn encode(n: u64) -> String {
    match n {
        0 => "zero".to_owned(),
        _ => {
            let mut num = n;
            let mut result: String = String::new();
            for u in (1u32..7).rev() {
                let denomination = THOUSAND.pow(u);
                let coefficient = num / denomination;
                num %= denomination;
                if coefficient > 0 {
                    result.push_str(format!("{} {}", encode_coefficient(coefficient), encode_denominations(denomination)).as_str());
                    if num > 0 {
                        result.push(' ');
                    }
                }
            }

            let hundreds_and_tens_and_ones = n % THOUSAND;
            if hundreds_and_tens_and_ones > 0 {
                result.push_str(encode_coefficient(n % THOUSAND).as_str());
            }
            result
        }
    }
}
