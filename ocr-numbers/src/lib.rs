use rayon::prelude::*;

// The code below is a stub. Just enough to satisfy the compiler.
// In order to pass the tests you can add-to or change any of this code.

#[derive(Debug, PartialEq, Eq)]
pub enum Error {
    InvalidRowCount(usize),
    InvalidColumnCount(usize),
}

fn recognize(rows: &[Vec<char>], i: usize, j: usize) -> char {
    match (
        rows[i][j],
        rows[i][j + 1],
        rows[i][j + 2],
        rows[i + 1][j],
        rows[i + 1][j + 1],
        rows[i + 1][j + 2],
        rows[i + 2][j],
        rows[i + 2][j + 1],
        rows[i + 2][j + 2],
        rows[i + 3][j],
        rows[i + 3][j + 1],
        rows[i + 3][j + 2],
    ) {
        (' ', '_', ' ', '|', ' ', '|', '|', '_', '|', ' ', ' ', ' ') => '0',
        (' ', ' ', ' ', ' ', ' ', '|', ' ', ' ', '|', ' ', ' ', ' ') => '1',
        (' ', '_', ' ', ' ', '_', '|', '|', '_', ' ', ' ', ' ', ' ') => '2',
        (' ', '_', ' ', ' ', '_', '|', ' ', '_', '|', ' ', ' ', ' ') => '3',
        (' ', ' ', ' ', '|', '_', '|', ' ', ' ', '|', ' ', ' ', ' ') => '4',
        (' ', '_', ' ', '|', '_', ' ', ' ', '_', '|', ' ', ' ', ' ') => '5',
        (' ', '_', ' ', '|', '_', ' ', '|', '_', '|', ' ', ' ', ' ') => '6',
        (' ', '_', ' ', ' ', ' ', '|', ' ', ' ', '|', ' ', ' ', ' ') => '7',
        (' ', '_', ' ', '|', '_', '|', '|', '_', '|', ' ', ' ', ' ') => '8',
        (' ', '_', ' ', '|', '_', '|', ' ', '_', '|', ' ', ' ', ' ') => '9',
        _ => '?',
    }
}

pub fn convert(input: &str) -> Result<String, Error> {
    let rows: Vec<Vec<char>> = input
        .split('\n')
        .map(|row| row.chars().collect::<Vec<char>>())
        .collect();
    if rows.len() % 4 != 0 {
        return Err(Error::InvalidRowCount(rows.len()));
    }

    if let Some(x) = rows.iter().map(|row| row.len()).find(|r| r % 3 != 0) {
        return Err(Error::InvalidColumnCount(x));
    }
    let m = rows.len() / 4;
    let n = rows[0].len() / 3;

    let tokens: Vec<String> = (0..m)
        .into_par_iter()
        .map(|i| {
            (0..n)
                .into_par_iter()
                .map(|j| recognize(&rows, i * 4, j * 3))
                .collect::<String>()
        })
        .collect();

    Ok(tokens.join(","))
}
