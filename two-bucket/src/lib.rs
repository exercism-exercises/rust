
#[derive(PartialEq, Eq, Debug)]
pub enum Bucket {
    One,
    Two,
}

/// A struct to hold your results in.
#[derive(PartialEq, Eq, Debug)]
pub struct BucketStats {
    /// The total number of "moves" it should take to reach the desired number of liters, including
    /// the first fill.
    pub moves: u8,
    /// Which bucket should end up with the desired number of liters? (Either "one" or "two")
    pub goal_bucket: Bucket,
    /// How many liters are left in the other bucket?
    pub other_bucket: u8,
}

fn gcd(x: u8, y: u8) -> u8 {
    if x == 0 {
        y
    } else {
        gcd(y % x, x)
    }
}

fn pour(from_capacity: u8, to_capacity: u8, goal: u8) -> (u8, u8, u8) {
    let mut from = from_capacity;
    let mut to = 0;
    let mut step = 1;

    if to_capacity == goal {
        step += 1;
        to = goal;
    }

    while from != goal && to != goal {
        let to_pour = to_capacity - to;
        let max_amount_to_pour = if from <= to_pour { from } else { to_pour };
        to += max_amount_to_pour;
        from -= max_amount_to_pour;
        step += 1;
        if from == goal || to == goal {
            return (step, from, to);
        }
        if from == 0 {
            from = from_capacity;
            step += 1;
        }
        if to == to_capacity {
            to = 0;
            step += 1;
        }
    }
    (step, from, to)
}

/**

fn extended_euclidean(x: u8, y: u8) -> (u8, u8, u8) {
    if x == 0 {
        return (y, 0, 1);
    }
    (gcd, a, b) = extended_euclidean(y % x, x);
    // gcd(x, y) = gcd(y % x, x) = a*(y%x) + b*x = a*(y - floor(y/x)*x) + b*x
    // ->
    //gcd(x, y) = (b-floor(y/x)*a)*x + a*y
    return (gcd, b - (y / x) * a, a);
}
 */

/// Solve the bucket problem
pub fn solve(
    capacity_1: u8,
    capacity_2: u8,
    goal: u8,
    start_bucket: &Bucket,
) -> Option<BucketStats> {
    let g = gcd(capacity_1, capacity_2);

    if goal % g == 0 && (goal <= capacity_1 || goal <= capacity_2) {
        Some(match start_bucket {
            Bucket::One => {
                let (step, from, to) = pour(capacity_1, capacity_2, goal);
                let is_from = from == goal;
                BucketStats {
                    moves: step,
                    goal_bucket: if is_from { Bucket::One } else { Bucket::Two },
                    other_bucket: if is_from { to } else { from },
                }
            }
            Bucket::Two => {
                let (step, from, to) = pour(capacity_2, capacity_1, goal);
                let is_from = from == goal;
                BucketStats {
                    moves: step,
                    goal_bucket: if is_from { Bucket::Two } else { Bucket::One },
                    other_bucket: if is_from { to } else { from },
                }
            }
        })
    } else {
        None
    }
}
