#[derive(Debug)]
pub struct ChessPosition(i32, i32);

#[derive(Debug)]
pub struct Queen {
    position: ChessPosition,
}

impl ChessPosition {
    pub fn new(rank: i32, file: i32) -> Option<Self> {
        match (rank, file) {
            (r, m) if 0 <= r && r <= 7 && 0 <= m && m <= 7 => Some(Self(rank, file)),
            _ => None,
        }
    }
}

impl Queen {
    pub fn new(position: ChessPosition) -> Self {
        Self { position: position }
    }

    pub fn can_attack(&self, other: &Queen) -> bool {
        self.position.0 == other.position.0
            || self.position.1 == other.position.1
            || (self.position.0 + self.position.1) == (other.position.0 + other.position.1)
            || (self.position.0 - self.position.1) == (other.position.0 - other.position.1)
    }
}
