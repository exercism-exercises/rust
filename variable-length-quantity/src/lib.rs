const SEVEN_BITS: u32 = 127u32;
const MSB: u8 = 128;

#[derive(Debug, PartialEq, Eq)]
pub enum Error {
    IncompleteNumber,
}

/// Convert a list of numbers to a stream of bytes encoded with variable length encoding.
/// Here is a worked-out example for the decimal number 137:
/// Represent the value in binary notation (e.g. 137 as 10001001)
/// Break it up in groups of 7 bits starting from the lowest significant bit (e.g. 137 as 0000001 0001001). This is equivalent to representing the number in base 128.
/// Take the lowest 7 bits, and that gives you the least significant byte (0000 1001). This byte comes last.
/// For all the other groups of 7 bits (in the example, this is 000 0001), set the MSb to 1 (which gives 1000 0001 in our example).
/// Thus 137 becomes 1000 0001 0000 1001, where the bits in boldface are something we added.
/// These added bits denote whether there is another byte to follow or not.
/// Thus, by definition, the very last byte of a variable-length integer will have 0 as its MSb.
pub fn to_bytes(values: &[u32]) -> Vec<u8> {
    let mut result: Vec<u8> = Vec::new();
    for &value in values {
        let mut num = value;
        let mut one_number: Vec<u8> = Vec::new();
        let last_byte: u8 = (num & SEVEN_BITS) as u8;
        one_number.push(last_byte);
        num >>= 7;
        while num > 0u32 {
            let byte: u8 = (num & SEVEN_BITS) as u8 | MSB;
            one_number.push(byte);
            num >>= 7;
        }
        for &byte in one_number.iter().rev() {
            result.push(byte);
        }
    }
    result
}

/// Given a stream of bytes, extract all numbers which are encoded in there.
pub fn from_bytes(bytes: &[u8]) -> Result<Vec<u32>, Error> {
    if bytes.len() == 1 && bytes[0] & MSB > 0 {
        return Err(Error::IncompleteNumber);
    }

    let mut result: Vec<u32> = Vec::new();
    let mut value: u32 = 0;
    for &byte in bytes {
        value += (byte & 127u8) as u32;

        if byte & MSB == 0 {
            result.push(value);
            value = 0;
        } else {
            value <<= 7;
        }
    }
    Ok(result)
}
