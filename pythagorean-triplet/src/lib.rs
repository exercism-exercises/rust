use std::collections::HashSet;

pub fn find(sum: u32) -> HashSet<[u32; 3]> {
    let mut set: HashSet<[u32; 3]> = HashSet::new();
    if sum <= 11 {
        return set;
    }
    for i in 3..(sum + 1) / 3 {
        for j in (i + 1)..(sum - i + 1) / 2 {
            let k = sum.saturating_sub(i).saturating_sub(j);
            let min = i.min(j).min(k);
            let max = i.max(j).max(k);
            let mid = sum - min - max;

            if min * min + mid * mid == max * max {
                set.insert([min, mid, max]);
            }
        }
    }
    set
}
