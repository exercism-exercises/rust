pub fn nth(n: u32) -> u32 {
    fn is_prime(m: u32, primes: &Vec<u32>) -> bool{
        primes.iter().all(|&x| m % x != 0)
    }

    let mut primes = Vec::<u32>::new();
    for i in 2.. {
        if is_prime(i, &primes) {
            primes.push(i)
        }
        if primes.len() == (n + 1) as usize {
            break;
        }
    }

    primes.pop().unwrap()
}
