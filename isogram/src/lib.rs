use std::collections::HashSet;

pub fn check(candidate: &str) -> bool {
    let mut set: HashSet<char> = HashSet::new();

    for c in candidate.chars() {
        match c {
            '-' => {continue;},
            c if c.is_whitespace() => {continue;},
            c if c.is_alphabetic() => {
                if set.contains(&c.to_ascii_lowercase()) {
                    return false
                }
                set.insert(c.to_ascii_lowercase());
            },
            _ => panic!()
        }
    }
    true
}
